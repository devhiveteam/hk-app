//
//  AchievementsManager.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/18/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

typedef void (^AchievementSubmittedHandler)(GKAchievement* achievement, NSError *error);

@interface AchievementsManager : NSObject
@property (nonatomic) NSInteger totalCoinsWon;
@property (nonatomic) NSInteger totalRoundsPlayed;
@property (nonatomic) NSInteger totalGamesPlayed;


+(AchievementsManager*)sharedManager;
- (void)updateTotalCoinsWon:(NSInteger)_totalCoinsWon withComletionHandler:(AchievementSubmittedHandler)handler;
- (void)updateTotalRoundsPlayed:(NSInteger)_totalRoundsPlayed withHandler:(AchievementSubmittedHandler)handler;
- (void)updateTotalGamesPlayed:(NSInteger)_totalGamesPlayed withCompletionHandler:(AchievementSubmittedHandler)handler;

- (void)userWonThreeOfAKind:(AchievementSubmittedHandler)handler;
- (void)userWon1000Coins:(AchievementSubmittedHandler)handler;
- (void)userWonWith7:(AchievementSubmittedHandler)handler;
- (void)userKilledQueen:(AchievementSubmittedHandler)handler;
- (void)firstRound:(AchievementSubmittedHandler)handler;
- (void)friendlyPair:(AchievementSubmittedHandler)handler;
- (void)userWonWith2:(AchievementSubmittedHandler)handler;
- (void)userLostWithKing:(AchievementSubmittedHandler)handler;
- (void)userBetMoreThan500:(AchievementSubmittedHandler)handler;
@end
