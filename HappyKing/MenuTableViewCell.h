//
//  MenuTableViewCell.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/06/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@end
