//
//  BetView.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/12/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "BetView.h"
#import "GCHelper.h"

typedef enum {
    ButtonPlus = 1,
    ButtonMinus,
    ButtonOK
}Button;

@interface BetView()
@property (nonatomic, weak) IBOutlet UIView *vwPopup;
@property (nonatomic, weak) IBOutlet UITextField *txtBets;
@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UIButton *btnPlus;
@property (nonatomic, weak) IBOutlet UIButton *btnMinus;
@property (nonatomic, weak) IBOutlet UIButton *btnOk;

@end

@implementation BetView

+ (BetView*)betView {
    BetView *view = [[NSBundle mainBundle] loadNibNamed:@"BetView" owner:nil options:nil][0];
    return view;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)initialize{
    self.txtBets.text = @"0";
    betCoins = 0;
    currentCoins = [[GCHelper sharedManager] coins];
}


- (IBAction)buttonPressed:(id)sender {
    UIButton *button = sender;
    switch (button.tag) {
        case ButtonMinus:
        {
            if (betCoins > 0)
            {
                if (betCoins % 5 != 0) {
                    betCoins -= (betCoins %5);
                } else {
                    betCoins -= 5;
                }
            } else {
                betCoins = 0;
            }
            [self updateCoinsInUI];
        }
            break;
        case ButtonPlus:
        {
            betCoins += 5;
            if (betCoins > currentCoins) {
                betCoins = currentCoins;
            }
            [self updateCoinsInUI];
        }
            break;
        case ButtonOK:
        {
            [self dismiss];
        }
            break;
    }
}

- (void)showInView:(UIView*)view {
    [self updateCoinsInUI];
    [self.vwPopup setTransform:CGAffineTransformMakeScale(0.01, 0.01)];
    [self setAlpha:0];
    [view addSubview:self];
    self.frame = view.frame;
    [UIView animateWithDuration:0.15 animations:^{
        [self setAlpha:1.0f];
        [self.vwPopup setTransform:CGAffineTransformMakeScale(1.2, 1.2)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 animations:^{
            [self.vwPopup setTransform:CGAffineTransformMakeScale(0.85, 0.85)];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.15 animations:^{
                [self.vwPopup setTransform:CGAffineTransformIdentity];
            } completion:^(BOOL finished) {
                
            }];
        }];
    }];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        
    } completion:^(BOOL finished) {
        
    }];
    [UIView animateWithDuration:0.10 animations:^{
        [self.vwPopup setTransform:CGAffineTransformMakeScale(1.1, 1.1)];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 animations:^{
            [self setTransform:CGAffineTransformMakeScale(0.01, 0.01)];
            [self setAlpha:0.0f];
        } completion:^(BOOL finished) {
            if (self.delegate) {
                [self.delegate betView:self didMakeBet:@(betCoins)];
            }
            [self removeFromSuperview];
            
        }];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *regex = @"^([0-9]+)?$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isValid = [test evaluateWithObject:newString];
    return isValid;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length == 0) {
        betCoins = 0;
    } else {
        betCoins = [textField.text intValue];
        if (betCoins > currentCoins) {
            betCoins = currentCoins;
        }
    }
    [self updateCoinsInUI];
}

- (void)updateCoinsInUI {
    [self.btnOk setEnabled:betCoins > 0];
    [self.txtBets setText:[@(betCoins) stringValue]];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if ([self.txtBets isFirstResponder]) {
        [self.txtBets resignFirstResponder];
    }
}

@end
