//
//  CardView.h
//  HappyKing
//
//  Created by Leonardo Cid on 28/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"

@interface CardView : UIView 
@property (nonatomic, weak) IBOutlet UIImageView *suitImageView;
@property (nonatomic, weak) IBOutlet UILabel *rankLabel;
@property (nonatomic, weak) IBOutlet UIView *backView;
@property (nonatomic) BOOL flipped;
@property (nonatomic, strong) Card *card;


+(CardView*)cardViewWithCard:(Card*)card;
@end
