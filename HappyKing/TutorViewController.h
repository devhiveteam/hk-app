//
//  TutorViewController.h
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 29/11/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  View Controller que muestra una pequeña guía acerca de las reglas del juego.
 */

@interface TutorViewController : UIViewController {
    /**
     *  Arreglo con los nombres de las imágenes que representan las instrucciones.
     */
    NSArray *instructions;
}

/**
 *  Scrollview donde se agregan las páginas con las instrucciones para el juego
 */
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

/**
 *  Botón que cierra la vista del tutorial.
 */
@property (nonatomic, weak) IBOutlet UIButton *quitTutorView;
/**
 *  Boton para desplazamiento hacia la izquierda
 */
@property (nonatomic, weak) IBOutlet UIButton *leftArrow;
/**
 *  Boton para desplazamiento hacia la derecha
 */
@property (nonatomic, weak) IBOutlet UIButton *rightArrow;

@end