//
//  MultiplayerNetworking.h
//  HappyKing
//
//  Created by Leonardo Cid on 02/09/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MultiplayerNetworkingProtocol <NSObject>

- (void)setCurrentPlayerIndex:(NSUInteger)index;

@end

@interface MultiplayerNetworking : NSObject

@property (nonatomic, strong) id<MultiplayerNetworkingProtocol> delegate;

@end
