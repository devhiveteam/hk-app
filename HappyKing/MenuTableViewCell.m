//
//  MenuTableViewCell.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/06/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
