//
//  MenuViewController.h
//  HappyKing
//
//  Created by Leonardo Cid on 30/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
/**
 Constantes que denotan las distintas secciones del menú
 */
typedef enum {
    /**
     *  Sección principal
     */
    SectionMain = 0,
    
    /**
     *  Sección de un solo jugador.
     */
    SectionSinglePlayer,
    /**
     *  Seccion de la tienda
     */
    SectionStore
} MenuSection;

/**
 Constantes que denotan las entradas del menu principal
 */
typedef enum {
    /**
     *  Un solo jugador
     */
    MenuSinglePlayer = 0,
    /**
     *  Juego Rápido
     */
    MenuQuickPlay,
    /**
     *  Multijugador
     */
    MenuMultiplayer,
    /**
     *  Tienda
     */
    MenuStore,
}MenuEntry;
/**
 *  El MenuViewController es el encargado de presentar al usuario con el menú de opciones desde donde se partirá
 * para poder realizar distintas acciones, ya sea jugar, ir a la tienda, o ir a las puntuaciones.
 */
@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, GKGameCenterControllerDelegate>
{
    BOOL isDetailMode;
    /**
     *  La sección actual del menú en donde se encuentra el usuario
     */
    MenuSection menuSection;
    
    NSInteger numberOfPlayers;
    
    /**
     *  Indica si el gamecenter está o no habilitado
     */
    BOOL _gameCenterEnabled;
    
    /**
     *  Arreglo que contiene la información de los productos descargados desde la tienda
     */
    NSArray *_products;
    
    /**
     *  Fuente con la que se despliega los textos de cada celda del menú.
     */
    UIFont *cellFont;
    // New instance variable
    
    /**
     *  View Controller que se está presentando
     */
    UIViewController *presentingViewController;
    
    /**
     *  Altura de la celda
     */
    float cellHeight;
    
    /**
     *  Arreglo que representa las distintas opciones disponibles del menú
     */
    NSArray *menuEntries;
}

/**
 *  La etiqueta que muestra el número de monedas que tiene el jugador
 */
@property (nonatomic, weak) IBOutlet UILabel *coinsLabel;
/**
 *  Contiene los identificadores de los productos disponibles en la tienda
 */
@property (nonatomic, strong) NSArray *storeItemsIds;
/**
 *  Tabla que muestra el menú
 */
@property (nonatomic, strong)IBOutlet UITableView *tableView;
/**
 *  Imagen de fondo
 */
@property (nonatomic, strong)IBOutlet UIImageView *backgroundImageView;

- (void)startMultiplayerMatch;
@end
