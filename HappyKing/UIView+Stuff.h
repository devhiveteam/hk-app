//
//  UIView+Stuff.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/25/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Stuff)

- (UIView *)keyView;

@end
