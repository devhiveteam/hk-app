//
//  MenuViewController.m
//  HappyKing
//
//  Created by Leonardo Cid on 30/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "GCHelper.h"
#import "InAppPurchasesHelper.h"
#import "PListData.h"
#import "Definitions.h"
#import "ThemesStoreViewController.h"
#import "UIView+Stuff.h"
#import "ScoreView.h"
#import "GCHelper.h"

@interface MenuViewController()
- (void)resetToken;
@end

@implementation MenuViewController
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [self layoutSubViews];
    
    UIFont *font = kMainFontWithSize(20);
    self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", (unsigned long)[[GCHelper sharedManager] coins]];
    self.coinsLabel.font = font;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    menuEntries = [NSArray arrayWithObjects:
                   @{@"title" : @"Un Jugador", @"type": @(MenuSinglePlayer)},
                   @{@"title" : @"Juego Rápido", @"type": @(MenuQuickPlay)},
                   @{@"title" : @"Multijugador", @"type": @(MenuMultiplayer)},
                   //@{@"title" : @"Tienda", @"type": @(MenuStore)},
                   nil];
    
    [self delegateEventTableThemeChanged];
    


    menuSection= SectionMain;
    /*[[GCHelper sharedManager] authenticateLocalPlayer:^(UIViewController *viewController, NSError *error) {
        if (viewController) {
            [self presentViewController:viewController animated:YES completion:^{
                
            }];
        }
    }];*/
    /*if (!self.secretPass) {
        [self registerUser];
    } else if (!self.token ) {
        [self getOauthToken];
    }*/
    //I'm verifying user is not authenticated on HK api
    GCHelper* helper = [GCHelper sharedManager];
    
    NSString* accessToken = helper.token;
    NSLog(@"Saved token: %@", accessToken);
    
    if(!accessToken){
        //I am not, let's change that
        if(!helper.uuid){
            [helper requestId:^{
                if(helper.uuid){
                    [helper registerUser:^{
                        [helper getOauthToken:^{
                            if (helper.token) {
                                //I'm in
                                [[NSUserDefaults standardUserDefaults] setValue:helper.token forKey:@"oauthToken"];
                                [[NSUserDefaults standardUserDefaults] setValue:helper.refreshToken forKey:@"oauthRefreshToken"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                [helper getUserInfo:^(id response){
                                    [[NSUserDefaults standardUserDefaults] setValue:response[@"coins"] forKey:@"numberOfCoins"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    [helper setCoins: [response[@"coins"] intValue]];
                                    self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", [[GCHelper sharedManager] coins]];
                                } onError:^{
                                    
                                }];
                            }
                        }];
                    }];
                }
            }];
        }
    } else {
        //Maybe I am, let's dig deeper
        [helper getUserInfo:^(id response){
            [[NSUserDefaults standardUserDefaults] setValue:response[@"coins"] forKey:@"numberOfCoins"];
            [helper setCoins: [response[@"coins"] intValue]];
            self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", [[GCHelper sharedManager] coins]];
        } onError:^{
            NSLog(@"Error getting coins, maybe expired token.");
            if (helper.refreshToken) {
                [helper refreshOauthToken:^{
                    if (helper.token) {
                        //Refreshed
                        [[NSUserDefaults standardUserDefaults] setValue:helper.token forKey:@"oauthToken"];
                        [[NSUserDefaults standardUserDefaults] setValue:helper.refreshToken forKey:@"oauthRefreshToken"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [helper getUserInfo:^(id response){
                            [[NSUserDefaults standardUserDefaults] setValue:response[@"coins"] forKey:@"numberOfCoins"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [helper setCoins: [response[@"coins"] intValue]];
                            self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", [[GCHelper sharedManager] coins]];
                        } onError:^{
                            [self resetToken];
                        }];
                    }
                }];
            } else {
                [self resetToken];
            }
        }];
    }
    [[GCHelper sharedManager] authenticateLocalPlayer:^(UIViewController *viewController, NSError *error) {
        if (viewController) {
            [self presentViewController:viewController animated:YES completion:^{
                
            }];
        }
    }];
    
}
/**
 *  Reinicia el token de acceso.
 */
- (void)resetToken {
    GCHelper* helper = [GCHelper sharedManager];
    
    NSString* accessToken = nil;
    
    if(!helper.uuid){
        [helper requestId:^{
            if(helper.uuid){
                [helper registerUser:^{
                    [helper getOauthToken:^{
                        if (helper.token) {
                            //I'm in
                            [[NSUserDefaults standardUserDefaults] setValue:helper.token forKey:@"oauthToken"];
                            [[NSUserDefaults standardUserDefaults] setValue:helper.refreshToken forKey:@"oauthRefreshToken"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [helper getUserInfo:^(id response){
                                [[NSUserDefaults standardUserDefaults] setValue:response[@"coins"] forKey:@"numberOfCoins"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                [helper setCoins: [response[@"coins"] intValue]];
                                self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", [[GCHelper sharedManager] coins]];
                            } onError:^{
                                
                            }];
                        }
                    }];
                }];
            }
        }];
    } else {
        [helper getOauthToken:^{
            if (helper.token) {
                //I'm in
                [[NSUserDefaults standardUserDefaults] setValue:helper.token forKey:@"oauthToken"];
                [[NSUserDefaults standardUserDefaults] setValue:helper.refreshToken forKey:@"oauthRefreshToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [helper getUserInfo:^(id response){
                    [[NSUserDefaults standardUserDefaults] setValue:response[@"coins"] forKey:@"numberOfCoins"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [helper setCoins: [response[@"coins"] intValue]];
                    self.coinsLabel.text = [NSString stringWithFormat:@"%lu Monedas", [[GCHelper sharedManager] coins]];
                } onError:^{
                    
                }];
            }
        }];
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self showScoresView];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ranFirstTime"] == nil){
        [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"ranFirstTime"];
        [self performSegueWithIdentifier:@"ToTutorial" sender:self];
    }
}

- (void)layoutSubViews {
    UIView *logo = [self.view viewWithTag:1];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat height = [[UIScreen mainScreen] bounds].size.height * .4;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:logo attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:height];
    [logo addConstraint:constraint];
    [logo setNeedsLayout];
    
    cellHeight = (bounds.size.height - height - 12) / 3;
    
    float width = (cellHeight + 10) * 600.0/204.0;
    constraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:width];
    [self.tableView addConstraint:constraint];
    [self.tableView setNeedsLayout];
    
    float curtainsHeight = bounds.size.width * .20;
    UIView *leftCurtains = [self.view viewWithTag:3];
    UIView *rightCurtains = [self.view viewWithTag:4];
    constraint = [NSLayoutConstraint constraintWithItem:leftCurtains attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:curtainsHeight];
    [leftCurtains addConstraint:constraint];
    constraint = [NSLayoutConstraint constraintWithItem:rightCurtains attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:curtainsHeight];
    [rightCurtains addConstraint:constraint];
    
    [leftCurtains setNeedsLayout];
    [rightCurtains setNeedsLayout];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch(menuSection) {
        case SectionSinglePlayer:
            return 6;
        case SectionMain:
            return [menuEntries count];
        case SectionStore:
            return _products.count + 1;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIFont *font = kMainFontWithSize(18);
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableCell"];
    cell.lblTitle.layer.cornerRadius = 5;
    cell.lblTitle.layer.masksToBounds = YES;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblTitle.font = font;
    if (!cellFont) {
        cellFont = cell.lblTitle.font;
    }
    cell.lblTitle.text = @"";
    UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
    [imageView setImage:[UIImage imageNamed:@"Menu_Boton_Blank.png"]];
    cell.lblTitle.highlightedTextColor = [UIColor colorWithRed:0.09 green:0.24 blue:0.1 alpha:1.0];
    if (menuSection == SectionSinglePlayer) {
        cell.lblTitle.font = cellFont;
        if (indexPath.row == 0) {
            cell.lblTitle.text = @"Atrás";
        } else {
            cell.lblTitle.text = [NSString stringWithFormat:@"%d Jugadores", (int)(4 + indexPath.row - 1)];
        }
        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
        [imageView setImage:[UIImage imageNamed:@"Menu_Boton_Blank.png"]];
    } else if (menuSection == SectionMain){
        cell.lblTitle.font = cellFont;
//        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
//        [imageView setImage:[UIImage imageNamed:menuEntries[indexPath.row]]];
        NSDictionary *dict = menuEntries[indexPath.row];
        switch ((MenuEntry)[dict[@"type"] intValue]) {
            case MenuSinglePlayer:
                cell.lblTitle.text = @"Un jugador";
                break;
            case MenuQuickPlay:
                cell.lblTitle.text = @"Juego Rápido";
                break;
            case MenuMultiplayer:
                cell.lblTitle.text = @"Multijugador";
                break;
            case MenuStore:
                cell.lblTitle.text = @"Tienda";
        }
    } else {
        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
        [imageView setImage:[UIImage imageNamed:@"Menu_Boton_Blank.png"]];
        if (indexPath.row == 0) {
            cell.lblTitle.font = cellFont;
            cell.lblTitle.text = @"Atrás";
        } else {
            cell.lblTitle.font = [font fontWithSize:12];
            SKProduct * product = (SKProduct *) _products[indexPath.row - 1];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [formatter setLocale:product.priceLocale];
            NSString *localizedMoneyString = [formatter stringFromNumber:product.price];
            cell.lblTitle.text = [NSString stringWithFormat:@"%@ - %@", product.localizedTitle, localizedMoneyString];
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch(menuSection) {
        case SectionSinglePlayer:
        {
            if (indexPath.row == 0) {
                menuSection = SectionMain;
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            } else {
                numberOfPlayers = 4 + indexPath.row - 1;
                [self performSegueWithIdentifier:@"ToMainGame" sender:self];
            }
        }
            break;
        case SectionMain:
        {
            if (indexPath.row == 0) {
                menuSection = SectionSinglePlayer;
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            } else if (indexPath.row == 1) {
                if ([GCHelper sharedManager].coins > 0) {
                    numberOfPlayers = 2;
                    [self performSegueWithIdentifier:@"ToMainGame" sender:self];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"" message:@"No cuentas con suficientes monedas para apostar." delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles: nil] show];
                }
            } else if (indexPath.row == 2) {
                /*if ([GCHelper sharedManager].gameCenterEnabled) {
                    [[GCHelper sharedManager]
                     findMatchWithMinPlayers:2 maxPlayers:2 viewController:self];
                }*/
//                [self performSegueWithIdentifier:@"ToMultiplayerRooms" sender:self];
                NSLog(@"Multi selected");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Happy King" message:@"¡Muy pronto!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            } else if (indexPath.row == 3) {
                menuSection = SectionStore;
                if (!_products) {
                    [[InAppPurchasesHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                        if (success) {
                            _products = products;
                            NSMutableArray *indexPaths = [NSMutableArray array];
                            for (int i = 0; i < products.count; i++) {
                                NSIndexPath *path = [NSIndexPath indexPathForRow:i+1 inSection:0];
                                [indexPaths addObject:path];
                            }
                            [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
                        }
//                        [self.refreshControl endRefreshing];
                    }];
                }
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
            break;
        case SectionStore:
        {
            if (indexPath.row == 0) {
                menuSection = SectionMain;
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            } else {
                SKProduct *product = _products[indexPath.row - 1];
                NSLog(@"Buying %@...", product.productIdentifier);
                [[InAppPurchasesHelper sharedInstance] buyProduct:product];
            }
        }
            break;
    }
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (menuSection == SectionMain) {
        if (indexPath.row == 2)
            return NO;
    }*/
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ToMainGame"]) {
        ViewController *gameViewController = segue.destinationViewController;
        if ([[GCHelper sharedManager] currentMatch]) {
            gameViewController.gameType = TypeMultiplayer;
            [GCHelper sharedManager].delegate = gameViewController;
        } else {
            if (numberOfPlayers == 2) {
                gameViewController.gameType = TypeQuick;
            } else {
                gameViewController.gameType = TypeSingle;
            }
            gameViewController.numberOfPlayer = numberOfPlayers;
        }
    }
}

- (IBAction)buttonPressed:(id)sender {
    if ([GCHelper sharedManager].gameCenterEnabled) {
        [self showLeaderboardAndAchievements:YES];
    } else {
        /*[[GCHelper sharedManager] authenticateLocalPlayer:^(UIViewController *viewController, NSError *error) {
            if (viewController) {
                [self presentViewController:viewController animated:YES completion:nil];
            }
        }];*/
    }
}

- (IBAction)shopButtonPressed:(id)sender {
    ThemesStoreViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesStoreViewController"];
    [storeVC setDelegate:self];
    [self presentViewController:storeVC animated:YES completion:nil];
}

-(void)delegateEventTableThemeChanged {
    NSString *ImageTableTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageTableTheme != nil)
    {
        [self.backgroundImageView setImage : [UIImage imageNamed:ImageTableTheme]];
    }
}

/**
 *  Muestra el panel de puntuaciones y logros de GameCenter
 *
 *  @param shouldShowLeaderboard <#shouldShowLeaderboard description#>
 */

-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard{
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    gcViewController.gameCenterDelegate = self;
    
    if (shouldShowLeaderboard) {
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
//        gcViewController.leaderboardIdentifier = [GCHelper sharedManager].leaderboardIdentifier;
    }
    else{
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
    }
    
    [self presentViewController:gcViewController animated:YES completion:nil];
}

/**
 *  Callback de finalización del View Controller de GameCenter
 *
 *  @param gameCenterViewController referencia al View Controller que se está presentando
 */

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

/**
 *  Notificacion de finalización de compra de un producto, dentro de éste método de lleva a cabo la actualización de las
 *  monedas una vez terminada la compra.
 *
 *  @param notification Referencia de la notificación que contiene la información con el producto comprado
 */

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    NSDictionary *userInfo = notification.userInfo;
    NSNumber *coinsNumber = userInfo[@"coins"];
    
    int coins = (int)[[GCHelper sharedManager] coins];
    coins += [coinsNumber integerValue];
    [[GCHelper sharedManager] setCoins:coins];
    [self.coinsLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[GCHelper sharedManager].coins]];
//    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
//        if ([product.productIdentifier isEqualToString:productIdentifier]) {
//            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
//            *stop = YES;
//        }
//    }];
    
}

/**
 *  Inicia una partida multijugador
 */
- (void)startMultiplayerMatch {
    [self performSegueWithIdentifier:@"ToMainGame" sender:self];
}

/**
 *  Muestra la vista de las puntuaciones.
 */

- (void)showScoresView {
    
    [[ScoreView sharedInstance] showInView:[self.view keyView]];
}

    
@end
