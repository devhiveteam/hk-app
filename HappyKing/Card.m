//
//  Card.m
//  HappyKing
//
//  Created by Leonardo Cid on 29/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "Card.h"



@implementation Card

- (id)initWithSuit:(Suit)suit andRank:(Rank)rank {
    if ((self = [super init])) {
        self.suit = suit;
        self.rank = rank;
    }
    return self;
}

+(Card*)createCardWithSuit:(Suit)suit andRank:(Rank)rank {
    return [[Card alloc] initWithSuit:suit andRank:rank];
}

- (BOOL)isBlack {
    return _suit == Cloves || _suit == Spades;
}

- (UIColor*)suitColor {
    UIColor *color = nil;
    if ([self isBlack]) {
        color = [UIColor blackColor];
    } else {
        color = [UIColor redColor];
    }
    return color;
}

- (NSString *)rankTitle {
    NSString *title = nil;
    switch(_rank) {
        case Ace:
            title = @"A";
            break;
        case Jack:
            title = @"J";
            break;
        case Queen:
            title = @"Q";
            break;
        case King:
            title = @"K";
            break;
        default:
            title = [@(_rank) stringValue];
    }
    return title;
}

- (UIImage *)suitImage {
    NSString *imgStr = nil;
    switch (_suit) {
        case Cloves:
            imgStr = @"Cloves";
            break;
        case Diamonds:
            imgStr = @"Diamonds";
            break;
        case Spades:
            imgStr = @"Spades";
            break;
        case Hearts:
            imgStr = @"Hearts";
            break;
    }
    imgStr = [imgStr stringByAppendingFormat:@"%02d", _rank];
    UIImage *image = [UIImage imageNamed:imgStr];
    return image;
}

@end
