//
//  Player.m
//  HappyKing
//
//  Created by Leonardo Cid on 29/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "Player.h"
#include <QuartzCore/QuartzCore.h>
#import "Definitions.h"

#define COLOR_TIMER_FULL [UIColor colorWithRed:0.43 green:0.67 blue:0.22 alpha:1.0];
#define COLOR_TIMER_HALF_FULL [UIColor colorWithRed:0.99 green:0.82 blue:0.04 alpha:1.0];
#define COLOR_TIMER_ALMOST_EMPTY [UIColor colorWithRed:0.83 green:0.12 blue:0.13 alpha:1.0];
#define TURN_TIME 15000

typedef enum {
    MessageIncrementLives,
    MessageDecrementLives,
} PlayerMessageType;

@interface Player ()
@property (nonatomic, strong) UIView *boardView;
@property (nonatomic, strong) UILabel *dealerLabel;
@property (nonatomic, strong) UILabel *livesLabel;
@property (nonatomic, strong) NSMutableArray *livesImages;
@property (nonatomic, strong) SFRoundProgressCounterView *counterView;
@property (nonatomic) float lastValue;
@property (nonatomic) GameType gameType;


@end

@implementation Player

- (id)initWithContainerview:(UIView *)containerview withGameType:(GameType)gameType{
    if ((self = [super init])) {
        self.gameType = gameType;
        _lives = gameType == TypeQuick  ? 1 : kPlayerInitialLives;
        [self initializeBoardView];
        [self setContainerView:containerview];

    }
    return self;
}

- (void)setPosition:(CGPoint)position {
    _position = position;
    CGRect rect = _boardView.frame;
    rect.origin = _position;
    [self.boardView setFrame:rect];
    [self.boardView removeFromSuperview];
    [self.containerView addSubview:self.boardView];
}

- (void)setCardView:(CardView *)cardView {
    if (cardView == nil) {
        [_cardView removeFromSuperview];
        _cardView = cardView;
        return;
    }
    CGRect rect = _cardView.frame;
    rect.origin = (CGPoint){(kPlayerBoardViewWidth - 60) / 2, (kPlayerBoardViewHeight - 60) / 2};
    rect.size = cardView.frame.size;
    CGRect fromRect = CGRectZero;
    CGRect toRect = CGRectZero;
    BOOL animate = NO;
    UIView *rootView = self.boardView.superview;
    if (!_cardView) {
        fromRect = [rootView convertRect:cardView.frame fromView:cardView.superview];
        if (_killMove) {
            fromRect = [cardView.superview convertRect:cardView.frame toView:rootView];
            toRect = CGRectMake((rootView.frame.size.width - cardView.frame.size.width)/2, (rootView.frame.size.height - cardView.frame.size.height)/2, cardView.frame.size.width, cardView.frame.size.height);
            
        } else {
            toRect = [rootView convertRect:rect fromView:self.boardView];
        }
//        toRect = [rootView convertRect:rect fromView:self.boardView];
        animate = YES;
    }
    BOOL switchCards = _cardView && _cardView != cardView;
    _cardView = cardView;
    if (animate) {
        
        _cardView.frame = fromRect;
        if (_killMove) {
            [self.boardView.superview addSubview:_cardView];
        } else {
            [rootView addSubview:_cardView];
        }
        [UIView animateWithDuration:CARD_ANIMATION animations:^{
            _cardView.frame = toRect;
            if (_killMove) {
                CGAffineTransform transform = CGAffineTransformMakeScale(2.0, 2.0);
                _cardView.transform = transform;
            }
        } completion:^(BOOL finished) {
            
            if (!_killMove) {
                [self.boardView addSubview:_cardView];
                _cardView.frame = rect;
            }
            if (self.isUser)
                self.cardView.flipped = YES;
            
        }];
    } else {

        if (!_killMove) {
            _cardView.frame = rect;
            [self.boardView addSubview:_cardView];
        }
    }
}

- (void)initializeBoardView {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Circle"]];
    self.boardView = [[UIView alloc] init];
    imageView.layer.cornerRadius = kPlayerBoardViewHeight / 2;
    [imageView setTag:1];
    [self.boardView addSubview:imageView];
//    self.boardView.layer.borderWidth = 1;
//    self.boardView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.boardView.layer.masksToBounds = YES;
    [self.boardView setFrame:(CGRect){{0,0},{kPlayerBoardViewWidth, kPlayerBoardViewHeight}}];
    [imageView setFrame:(CGRect){{0,0},{kPlayerBoardViewWidth, kPlayerBoardViewHeight}}];
    _dealerLabel = [[UILabel alloc] initWithFrame:(CGRect){{kPlayerBoardViewWidth - 15,kPlayerBoardViewHeight - 20},{15,16}}];
    _dealerLabel.font = kMainFontWithSize(14);
    _dealerLabel.text = @"D";
    _dealerLabel.textColor = [UIColor colorWithRed:1.0 green:0.1 blue:0.1 alpha:1];
    _dealerLabel.hidden = YES;
    [self.boardView addSubview:_dealerLabel];
    
    [self setUpLivesImages];
    
//    _livesLabel = [[UILabel alloc] initWithFrame:(CGRect){{kPlayerBoardViewWidth - 15,5},{15,16}}];
//    _livesLabel.font = [UIFont fontWithName:@"QuasixTitling-Regular" size:20];
//    _livesLabel.textColor = [UIColor colorWithRed:0.8 green:0.2 blue:0.2 alpha:1];
//    [self.boardView addSubview:_livesLabel];
    
    _counterView = [[SFRoundProgressCounterView alloc] initWithFrame:(CGRect){{0,0},{kPlayerBoardViewWidth, kPlayerBoardViewHeight}}];
    
    self.counterView.outerCircleThickness = @(8.0);
    self.counterView.innerCircleThickness = @(0.0);
    self.counterView.outerProgressColor = COLOR_TIMER_FULL;
    self.counterView.backgroundColor = [UIColor colorWithRed:0.1 green:0.47 blue:0.27 alpha:0.8];
    self.counterView.hideCounterLabel = YES;
    self.counterView.delegate = self;
    self.counterView.tintColor = [UIColor clearColor];
    self.counterView.hidden = YES;
    self.counterView.layer.cornerRadius = kPlayerBoardViewWidth / 2;
    
    UILabel *namelabel = [[UILabel alloc] initWithFrame:(CGRect){{0,0},{kPlayerBoardViewWidth,25}}];
    [namelabel setFont:kMainFontWithSize(18)];
    [namelabel setTextAlignment:NSTextAlignmentCenter];
    [namelabel setTag:2];
    [namelabel setTextColor:[UIColor lightGrayColor]];
    [self.boardView addSubview:namelabel];
    namelabel.center = imageView.center;
    
    [self.boardView insertSubview:_counterView atIndex:1];
    
    [self updateLivesInUI];
}

- (void)updateLivesInUI {
    
    int i = 0;
    for (UIImageView *liveImageView in self.livesImages) {
        liveImageView.hidden = !(i < self.lives);
        i++;
    }
//    self.livesLabel.text = [@(self.lives) stringValue];
}

- (void)setIsDealer:(BOOL)isDealer {
    _isDealer = isDealer;
    _dealerLabel.hidden = !isDealer;
}

- (void)setIsUser:(BOOL)isUser {
    _isUser = isUser;
    if (_isUser) {
        self.playerName = @"Jugador";
        self.counterView.hidden = NO;
    }
}

- (Player*)nextPlayer {
    if (_nextPlayer.lives)
        return _nextPlayer;
    Player *tempNextPlayer = _nextPlayer;
    do{
        tempNextPlayer = tempNextPlayer.nextPlayer;
        if (tempNextPlayer.lives)
            break;
    }while(1);
    return tempNextPlayer;
}

- (void)setIsMyTurn:(BOOL)isMyTurn {
    _isMyTurn = isMyTurn;
    UIImageView *imageView = (UIImageView*)[self.boardView viewWithTag:1];
    imageView.backgroundColor = _isMyTurn ? [UIColor colorWithRed:0.1 green:0.8 blue:0.45 alpha:1] : [UIColor clearColor];
}

- (BOOL)switchCardWithPlayer:(Player*)secondPlayer {
    CardView *myCardView = self.cardView;
    CardView *secondCardView = secondPlayer.cardView;
    
    Rank previousRank = self.cardView.card.rank;
    Rank afterRank = secondPlayer.cardView.card.rank;
    if (self.isUser) {
        secondCardView.flipped = YES;
        myCardView.flipped = NO;
    } else if (secondPlayer.isUser) {
        secondCardView.flipped = NO;
        myCardView.flipped = YES;
    }
    
    UIView *rootview = self.boardView.superview;
    
    CGRect selfCardViewRect = [self.boardView convertRect:myCardView.frame toView:rootview];
    CGRect switchCardViewRect = [secondPlayer.boardView convertRect:secondCardView.frame toView:rootview];
    
    myCardView.frame = selfCardViewRect;
    secondCardView.frame = switchCardViewRect;
    
    [rootview addSubview:myCardView];
    [rootview addSubview:secondCardView];
    
    [UIView animateWithDuration:CARD_ANIMATION animations:^{
        myCardView.frame = switchCardViewRect;
        secondCardView.frame = selfCardViewRect;
    } completion:^(BOOL finished) {
        self.cardView = secondCardView;
        secondPlayer.cardView = myCardView;
    }];
    
    //    [firstPlayer.cardView removeFromSuperview];
    //    [secondPlayer.cardView removeFromSuperview];
    self.shouldChange = NO;
    if (previousRank < afterRank && !secondPlayer.isUser) {
        NSLog(@"Bla Bla");
    }
    
    [secondPlayer setShouldChange:previousRank < afterRank && !secondPlayer.isUser && previousRank != Queen];
    return afterRank < previousRank;
}

- (void)setFinalState:(FinalState)finalState {
    _finalState = finalState;
    CABasicAnimation *animation = nil;
    UIImageView *imageView = (UIImageView*)[self.boardView viewWithTag:1];
    switch (finalState) {
        case StateHasKing:
        {
            animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
            animation.fromValue = (id)[UIColor clearColor].CGColor;
            animation.toValue = (id)[UIColor colorWithRed:0.33 green:0.55 blue:0.05 alpha:0.88].CGColor;
            animation.repeatCount = INFINITY;
            animation.duration = 0.5;
            animation.autoreverses = YES;
        }
            break;

        case StateSameFourOfAKind:
        {
            imageView.backgroundColor = [UIColor colorWithRed:0.1 green:0.2 blue:0.9 alpha:0.6]; //one life more
            [self incrementOneLife];
        }
            break;
        case StateLowest:
        {
            imageView.backgroundColor = [UIColor colorWithRed:1.0 green:0.1 blue:0.1 alpha:0.6]; //one life less
            [self decrementOneLife];
        }
            break;
        case StateSamePair:
        {
            imageView.backgroundColor = [UIColor colorWithRed:0.1 green:0.9 blue:0.4 alpha:0.6]; //remains the same
        }
            break;
        case StateSameThreeOfAKind:
        {
            imageView.backgroundColor = [UIColor colorWithRed:1.0 green:0.1 blue:0.1 alpha:0.6]; //one life less
            [self decrementOneLife];
        }
            break;
        case StateOut:
        {
            imageView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
            
            [[self delegate] playerDidLost:self];
        }
            break;
        case StateWinner:
        {
            animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
            animation.fromValue = (id)[UIColor clearColor].CGColor;
            animation.toValue = (id)[UIColor colorWithRed:0.1 green:0.2 blue:0.9 alpha:0.6].CGColor;
            animation.repeatCount = INFINITY;
            animation.duration = 0.5;
            animation.autoreverses = YES;
        }
            break;
        default:
            [imageView.layer removeAnimationForKey:kPlayerFinalStateAnimationKey];
            imageView.backgroundColor = [UIColor clearColor];
            break;
    }
    [self updateLivesInUI];
    if (animation) {
        [imageView.layer addAnimation:animation forKey:kPlayerFinalStateAnimationKey];
    }
    if (_lives == 0 && finalState != StateOut) {
        self.cardView = nil;
        self.finalState = StateOut;
    }
}

- (void)reset {
    self.finalState = StateUndefined;
    self.cardView = nil;
    self.isMyTurn = NO;
    self.changedCard = NO;
    self.killMove = NO;
    self.shouldChange = NO;
}

- (void)revealCard {
    self.cardView.flipped = YES;
}

- (BOOL)canCardBeSwapped {
    return self.cardView.card.rank != Queen;
}

- (void)showQueen {
    __block UIImageView *imageView = (UIImageView*)[self.boardView viewWithTag:1];
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionAutoreverse animations:^{
        imageView.backgroundColor = [UIColor colorWithRed:0.7 green:0.1 blue:0.2 alpha:1];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)setPlayerName:(NSString *)playerName {
    _playerName = playerName;
    UILabel *label = (UILabel *)[self.boardView viewWithTag:2];
    [label setText:playerName];
}

- (void)startTimerForTurn {
    _counterView.intervals=@[@(TURN_TIME)];
    colorFullChanged = false;
    colorHalfChanged = false;
    colorEmptyChanged = false;
    self.lastValue = 1.0f;
    [_counterView start];
    
}

- (void)stopTimerForTurn {
    [_counterView stop];
    [_counterView reset];
    self.counterView.outerProgressColor = COLOR_TIMER_FULL;
    colorFullChanged = false;
    colorHalfChanged = false;
    colorEmptyChanged = false;
    
}

- (void)countdownDidEnd:(SFRoundProgressCounterView*)progressCounterView {
    [self.delegate playerTurnDidEnd:self];
}

- (void)intervalDidEnd:(SFRoundProgressCounterView*)progressCounterView WithIntervalPosition:(int)position {
    
}

- (void)counter:(SFRoundProgressCounterView *)progressCounterView didReachValue:(unsigned long long)value {
    float percent = (float)value / (float)TURN_TIME;
    
    if (percent > 0.6) {
        if (!colorFullChanged) {
            colorFullChanged = YES;
            self.counterView.outerProgressColor = COLOR_TIMER_FULL;
        }
    } else if (percent >= 0.2){
        if (!colorHalfChanged) {
            colorHalfChanged = YES;
            self.counterView.outerProgressColor = COLOR_TIMER_HALF_FULL;
        }
    } else {
        if (!colorEmptyChanged) {
            colorEmptyChanged = YES;
            self.counterView.outerProgressColor = COLOR_TIMER_ALMOST_EMPTY;
        }
    }
}

- (void)incrementOneLife {
    self.lives++;
    [self showLivesMessage:MessageIncrementLives];
    if (self.lives > kPlayerInitialLives) {
        self.lives = kPlayerInitialLives;
    }
    [self updateLivesInUI];
}

- (void)decrementOneLife {
    self.lives--;
    [self showLivesMessage:MessageDecrementLives];
    if (self.lives < 0) {
        self.lives = 0;
    }
    [self updateLivesInUI];
}

- (NSString*)messageForType:(PlayerMessageType)type {
    switch (type) {
        case MessageDecrementLives:
            return @"-1";
            
        case MessageIncrementLives:
            return @"+1";
    }
}

- (UIColor*)colorWithMessageType:(PlayerMessageType)type {
    switch (type) {
        case MessageIncrementLives:
            return [UIColor greenColor];
        case MessageDecrementLives:
            return [UIColor redColor];
    }
}

- (void)showLivesMessage:(PlayerMessageType)type {
    __block UILabel *label = [[UILabel alloc] initWithFrame:(CGRect){{0,kPlayerBoardViewHeight - 20},{kPlayerBoardViewWidth,40}}];
    UIView *centerView = [self.boardView viewWithTag:1];
    label.center = centerView.center;
    [label setFont:kMainFontWithSize(25)];
    [label setTextColor:[self colorWithMessageType:type]];
    [label setText:[self messageForType:type]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:0];
    [self.boardView addSubview:label];
    label.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:1.0 animations:^{
        label.transform = CGAffineTransformMakeScale(1.5, 1.5);
        label.transform = CGAffineTransformTranslate(label.transform, 0.0, -30.0);
    } completion:^(BOOL finished) {
        [label removeFromSuperview];
        
    }];
}

- (void)setUpLivesImages {
    self.livesImages = [NSMutableArray array];
    
    UIImageView *imageView = nil;
    if (self.gameType != TypeQuick) {
        imageView = [[UIImageView alloc]initWithFrame:(CGRect){{kPlayerBoardViewWidth - 2*kPlayerCrownWidth,10},{kPlayerCrownWidth,kPlayerCrownHeight}}];
        [imageView setImage:[UIImage imageNamed:@"Menu_Popup_Campo_Selección_Check"]];
        [imageView setTintColor:[UIColor orangeColor]];
        [self.livesImages addObject:imageView];
    }
    imageView = [[UIImageView alloc]initWithFrame:(CGRect){{kPlayerBoardViewWidth - 1.5*kPlayerCrownWidth,2},{kPlayerCrownWidth,kPlayerCrownHeight}}];
    [imageView setImage:[UIImage imageNamed:@"Menu_Popup_Campo_Selección_Check"]];
    [imageView setTintColor:[UIColor orangeColor]];
    [self.livesImages addObject:imageView];
    if(self.gameType != TypeQuick) {
        imageView = [[UIImageView alloc]initWithFrame:(CGRect){{kPlayerBoardViewWidth - kPlayerCrownWidth,10},{kPlayerCrownWidth,kPlayerCrownHeight}}];
        [imageView setImage:[UIImage imageNamed:@"Menu_Popup_Campo_Selección_Check"]];
        [imageView setTintColor:[UIColor orangeColor]];
        [self.livesImages addObject:imageView];
    }
    for (UIView *view in self.livesImages) {
        [self.boardView addSubview:view];
    }

}

- (void)resetToInitialState {
    _lives = (_gameType == TypeQuick  ? 1 : kPlayerInitialLives);
    [self reset];
}

@end
