//
//  AchievementsManager.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/18/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "AchievementsManager.h"
#import "GCHelper.h"
#import <GameKit/GameKit.h>




#define ACHIEVEMENT_QUICK_EARN_1000_COINS_ID    @"com.happyking.ach.quick.1000coins"        //Done
#define ACHIEVEMENT_QUICK_BET_MORE_THAN_500_ID  @"com.happyking.ach.quick.apuesta_mas500"   //Done
#define ACHIEVEMENT_SINGLE_DAMNED_THIRD_ID      @"com.happyking.ach.single.tercia"          //Done
#define ACHIEVEMENT_SINGLE_WIN_WITH_SEVEN_ID    @"com.happyking.ach.single.ganar7"          //Done
#define ACHIEVEMENT_SINGLE_200_ROUNDS_ID        @"com.happyking.ach.single.rondas200"       //Done
#define ACHIEVEMENT_SINGLE_KILL_QUEEN_ID        @"com.happyking.ach.single.matasDama"       //Done
#define ACHIEVEMENT_MULTI_FIRST_ROUND_ID        @"com.happyking.ach.multi.primer_ronda"     //Done
#define ACHIEVEMENT_MULTI_FRIENDLY_PAIR_ID      @"com.happyking.ach.multi.par_amigos"       //Done
#define ACHIEVEMENT_MULTI_ONLY_LIFE_ID          @"com.happyking.ach.multi.sola_vida"        //
#define ACHIEVEMENT_MULTI_KING_LOSES_ID         @"com.happyking.ach.multi.perder_rey"       //Done
#define ACHIEVEMENT_GENERAL_500_TIMES_ID        @"com.happyking.ach.general.500veces"       //Done
#define ACHIEVEMENT_GENERAL_1000_TIMES_ID       @"com.happyking.ach.general.1000veces"      //Done
#define ACHIEVEMENT_GENERAL_2000_TIMES_ID       @"com.happyking.ach.general.2000veces"      //Done
#define ACHIEVEMENT_GENERAL_5000_TIMES_ID       @"com.happyking.ach.general.5000veces"      //Done
#define ACHIEVEMENT_GENERAL_WIN_WITH_TWO_ID     @"com.happyking.ach.quick.ganar_con2"       //Done

@interface AchievementsManager ()
@property (nonatomic, strong) NSMutableDictionary *earnedAchievementCache;
@end

@implementation AchievementsManager
@synthesize totalCoinsWon;
@synthesize totalGamesPlayed;
@synthesize totalRoundsPlayed;

+(AchievementsManager*)sharedManager {
    static AchievementsManager *instance;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [[AchievementsManager alloc] init];
            [instance initializeAchievements];
        }
    });
    return instance;
}

- (void)updateTotalCoinsWon:(NSInteger)_totalCoinsWon withComletionHandler:(AchievementSubmittedHandler)handler{
    [self setTotalCoinsWon: _totalCoinsWon];
    if (self.totalCoinsWon >= 1000) {
        [self userWon1000Coins:handler];
    }
}

- (void)updateTotalRoundsPlayed:(NSInteger)_totalRoundsPlayed withHandler:(AchievementSubmittedHandler)handler{
    [self setTotalRoundsPlayed: _totalRoundsPlayed];
    if (totalRoundsPlayed == 1) {
        [self firstRound:handler];
        handler = nil;
    }
    double percent = totalRoundsPlayed / 200.0f;
    [self submitAchievement:ACHIEVEMENT_SINGLE_200_ROUNDS_ID percentComplete:percent withCompletionHandler:handler];
    
}

- (void)updateTotalGamesPlayed:(NSInteger)_totalGamesPlayed withCompletionHandler:(AchievementSubmittedHandler)handler{
    [self  setTotalGamesPlayed: _totalGamesPlayed];
    NSString *achId = nil;
    double percent = 0;
    if (totalGamesPlayed <= 500) {
        achId = ACHIEVEMENT_GENERAL_500_TIMES_ID;
        percent = totalGamesPlayed;
    } else if (totalGamesPlayed <= 1000) {
        achId = ACHIEVEMENT_GENERAL_1000_TIMES_ID;
        percent = totalGamesPlayed;
    } else if (totalGamesPlayed <= 2000) {
        achId = ACHIEVEMENT_GENERAL_2000_TIMES_ID;
        percent = totalGamesPlayed;
    } else if (totalGamesPlayed <= 5000) {
        achId = ACHIEVEMENT_GENERAL_5000_TIMES_ID;
        percent = totalGamesPlayed;
    }
    if (achId) {
        [self submitAchievement:achId percentComplete:percent withCompletionHandler:handler];
    }
}

- (void)initializeAchievements {
    NSNumber *_totalGamesPlayed = [[NSUserDefaults standardUserDefaults] objectForKey:@"kTotalGamesPlayed"];
    [self setTotalGamesPlayed:[_totalGamesPlayed intValue]];
    NSNumber *_totalCoinsWon = [[NSUserDefaults standardUserDefaults] objectForKey:@"kTotalCoinsWon"];
    [self setTotalCoinsWon:[_totalCoinsWon intValue]];
    NSNumber *_totalRoundsPlayed = [[NSUserDefaults standardUserDefaults] objectForKey:@"kTotalRoundsPlayed"];
    [self setTotalGamesPlayed:[_totalRoundsPlayed intValue]];
//    if (![[GCHelper sharedManager] gameCenterEnabled]) {
//        return;
//    }
//    [GKAchievement loadAchievementsWithCompletionHandler: ^(NSArray *scores, NSError *error)
//     {
//         if(error == NULL)
//         {
//             NSMutableDictionary* tempCache= [NSMutableDictionary dictionaryWithCapacity: [scores count]];
//             for (GKAchievement* score in scores)
//             {
//                 [tempCache setObject: score forKey: score.identifier];
//             }
//             self.earnedAchievementCache= tempCache;
//         }
//         else
//         {
//             
//         }
//         
//     }];
}

- (void)setTotalCoinsWon:(NSInteger)_totalCoinsWon {
    totalCoinsWon = _totalCoinsWon;
    [[NSUserDefaults standardUserDefaults] setObject:@(_totalCoinsWon) forKey:@"kTotalCoinsWon"];
}

- (void)setTotalGamesPlayed:(NSInteger)_totalGamesPlayed {
    totalGamesPlayed = _totalGamesPlayed;
    [[NSUserDefaults standardUserDefaults] setObject:@(_totalGamesPlayed) forKey:@"kTotalGamesPlayed"];
}

- (void)totalRoundsPlayed:(NSInteger)_totalRoundsPlayed {
    totalRoundsPlayed = _totalRoundsPlayed;
    [[NSUserDefaults standardUserDefaults] setObject:@(_totalRoundsPlayed) forKey:@"kTotalRoundsPlayed"];
}

- (void)userWonThreeOfAKind:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_SINGLE_DAMNED_THIRD_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)userWon1000Coins:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_QUICK_EARN_1000_COINS_ID percentComplete:self.totalCoinsWon / 1000.0f withCompletionHandler:handler];
}

- (void)userWonWith7:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_SINGLE_WIN_WITH_SEVEN_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)userKilledQueen:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_SINGLE_KILL_QUEEN_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)firstRound:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_MULTI_FIRST_ROUND_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)friendlyPair:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_MULTI_FRIENDLY_PAIR_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)userWonWith2:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_GENERAL_WIN_WITH_TWO_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)userBetMoreThan500:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_QUICK_BET_MORE_THAN_500_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void)userLostWithKing:(AchievementSubmittedHandler)handler {
    [self submitAchievement:ACHIEVEMENT_MULTI_KING_LOSES_ID percentComplete:100.0f withCompletionHandler:handler];
}

- (void) submitAchievement: (NSString*) identifier percentComplete: (double) percentComplete withCompletionHandler:(AchievementSubmittedHandler)handler
{
    //GameCenter check for duplicate achievements when the achievement is submitted, but if you only want to report
    // new achievements to the user, then you need to check if it's been earned
    // before you submit.  Otherwise you'll end up with a race condition between loadAchievementsWithCompletionHandler
    // and reportAchievementWithCompletionHandler.  To avoid this, we fetch the current achievement list once,
    // then cache it and keep it updated with any new achievements.
    if (![[GCHelper sharedManager] gameCenterEnabled]) {
        return;
    }
    
    if(self.earnedAchievementCache == NULL)
    {
        [GKAchievement loadAchievementsWithCompletionHandler: ^(NSArray *scores, NSError *error)
         {
             if(error == NULL)
             {
                 NSMutableDictionary* tempCache= [NSMutableDictionary dictionaryWithCapacity: [scores count]];
                 for (GKAchievement* score in scores)
                 {
                     [tempCache setObject: score forKey: score.identifier];
                 }
                 self.earnedAchievementCache= tempCache;
                 [self submitAchievement: identifier percentComplete: percentComplete withCompletionHandler:handler];
             }
             else
             {
                 handler(nil,error);
             }
             
         }];
    }
    else
    {
        //Search the list for the ID we're using...
        GKAchievement* achievement= [self.earnedAchievementCache objectForKey: identifier];
        if(achievement != NULL)
        {
            if((achievement.percentComplete >= 100.0) || (achievement.percentComplete >= percentComplete))
            {
                //Achievement has already been earned so we're done.
                achievement= NULL;
            }
            achievement.percentComplete= percentComplete;
        }
        else
        {
            achievement= [[GKAchievement alloc] initWithIdentifier: identifier];
            achievement.percentComplete= percentComplete;
            achievement.showsCompletionBanner = YES;
            //Add achievement to achievement cache...
            [self.earnedAchievementCache setObject: achievement forKey: achievement.identifier];
        }
        if(achievement!= NULL)
        {
            //Submit the Achievement...
            [GKAchievement reportAchievements:@[achievement] withCompletionHandler:^(NSError * _Nullable error) {
//                GKNotificationBanner
                if (handler) {
                    handler(achievement, error);
                }
            }];
        }
    }
}



@end
