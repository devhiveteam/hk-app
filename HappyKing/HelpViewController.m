//
//  HelpViewController.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/24/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "HelpViewController.h"
#import "PListData.h"
#import "Definitions.h"

@interface HelpViewController ()
@property (nonatomic, weak) IBOutlet UIButton *menuButton;
@property (nonatomic, weak) IBOutlet UITextView *helpTextView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;

@end

@implementation HelpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *image = [UIImage imageNamed:@"Mesa01_Boton_Activo.png"];
    NSString *ImageButtonTheme = [PListData getValueWithKey:@"button" fromElement:@"Buttons" fromFile:@"themes"];
    if(ImageButtonTheme != nil)
    {
        image = [UIImage imageNamed:ImageButtonTheme];
    }
    [self.menuButton setBackgroundImage:image forState:UIControlStateNormal];
    [[self.menuButton titleLabel] setFont:kMainFontWithSize(16)];
    [self.titleLabel setFont:kMainFontWithSize(24)];
    [self.helpTextView setText:NSLocalizedString(@"HELP_TEXT", nil)];
    [self.helpTextView setFont:kMainFontWithSize(18)];
    [self.helpTextView setTextColor:[UIColor whiteColor]];
    
    NSString *ImageMesaTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageMesaTheme != nil)
    {
        [self.backgroundImageView setImage : [UIImage imageNamed:ImageMesaTheme]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
