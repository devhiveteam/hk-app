//
//  Player.h
//  HappyKing
//
//  Created by Leonardo Cid on 29/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardView.h"
#import <SFRoundProgressCounterView/SFRoundProgressCounterView.h>
#import <GameKit/GameKit.h>
#import "Definitions.h"

#define kPlayerBoardViewWidth 70
#define kPlayerBoardViewHeight 70
#define kPlayerUserBottomSpace 40
#define kPlayerBoardMargin 5
#define kPlayerFinalStateAnimationKey @"finalStateAnimationKey"
#define kPlayerInitialLives 3

#define kPlayerCrownWidth 12
#define kPlayerCrownHeight 8

#define CARD_ANIMATION 0.55

typedef enum {
    StateUndefined = 0,
    StateLowest,
    StateSamePair,
    StateSameThreeOfAKind,
    StateSameFourOfAKind,
    StateHasKing,
    StateOut,
    StateWinner,
}FinalState;

@class Player;

@protocol PlayerDelegate <NSObject>
- (void)playerDidLost:(Player*)player;
@optional
- (void)playerTurnDidEnd:(Player*)player;
@end

@interface Player : NSObject <SFRoundProgressCounterViewDelegate>
{
    BOOL colorFullChanged;
    BOOL colorHalfChanged;
    BOOL colorEmptyChanged;
}

@property (nonatomic, strong) Player *previousPlayer;
@property (nonatomic, strong) Player *nextPlayer;
@property (nonatomic) NSUInteger rightPlayerIndex; //next player
@property (nonatomic) NSUInteger leftPlayerIndex; //previousPlayer
@property (nonatomic) BOOL changedCard;
@property (nonatomic) CGPoint position;
@property (nonatomic, strong) CardView *cardView;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic) BOOL isDealer;
@property (nonatomic) BOOL isUser;
@property (nonatomic) BOOL isMyTurn;
@property (nonatomic) FinalState finalState;
@property (nonatomic) NSInteger lives;
@property (nonatomic, weak) id<PlayerDelegate> delegate;
@property (nonatomic, strong) NSString *playerName;
@property (nonatomic, strong) GKPlayer *gcPlayer;
@property (nonatomic) BOOL shouldChange;
@property (nonatomic) BOOL killMove;

- (id)initWithContainerview:(UIView *)containerview withGameType:(GameType)gameType;
//- (id)initWithContainerview:(UIView *)containerview;
- (BOOL)switchCardWithPlayer:(Player*)secondPlayer;
- (void)reset;
- (void)revealCard;
- (BOOL)canCardBeSwapped;
- (void)startTimerForTurn;
- (void)stopTimerForTurn;
- (void)incrementOneLife;
- (void)showQueen;

@end
