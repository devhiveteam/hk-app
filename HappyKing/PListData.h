//
//  PListData.h
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 26/09/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PListData : NSObject

+ (void)initialize:(NSString *)file;

+ (NSArray *)getItemData:(NSString *)key fromFile:(NSString *) fileName;

+ (NSDictionary *)getActiveItemWithKey:(NSString *)key fromFile:(NSString *)fileName;

+ (NSString *)getValueWithKey:(NSString *)key fromElement:(NSString *)element fromFile:(NSString *)fileName;

+ (void)saveActiveItemForKey:(NSString *)key withName:(NSString *)name fromFile:(NSString *)fileName;

+ (void)saveUnlockedItemForKey:(NSString *)key withName:(NSString *)name fromFile:(NSString *)fileName;

+ (NSNumber *)getVersionFromFile:(NSString *)fileName;

@end
