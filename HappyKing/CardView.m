//
//  CardView.m
//  HappyKing
//
//  Created by Leonardo Cid on 28/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "CardView.h"
#import <QuartzCore/QuartzCore.h>

#import "PListData.h"

@interface CardView()

@end

@implementation CardView 

+(CardView*)cardViewWithCard:(Card*)card {
    NSBundle *bundle = [NSBundle mainBundle];
    CardView *cardView = (CardView*)[bundle loadNibNamed:@"CardView" owner:self options:nil][0];
    cardView.card = card;
    return cardView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 5;
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1;
    self.layer.masksToBounds = YES;
}

- (void)setCard:(Card *)card {
    _card = card;
    [self updateUI];
}

- (void)updateUI {
    self.rankLabel.text = [_card rankTitle];
    self.rankLabel.textColor = [_card suitColor];
    self.suitImageView.image = [_card suitImage];
    
    NSString *ImageCardTheme = [PListData getValueWithKey:@"card" fromElement:@"Cards" fromFile:@"themes"];
    if(ImageCardTheme != nil)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:ImageCardTheme]];
        [imageView setFrame:CGRectMake(1, 1, 40, 60)];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
        [self.backView addSubview: imageView];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setFlipped:(BOOL)flipped {
    if (_flipped != flipped) {
        _flipped = flipped;
        [UIView transitionWithView:self
                          duration:0.25
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                            if (_flipped) {
                                [_backView setHidden:YES];
                            } else {
                                [_backView setHidden:NO];
                            }
                        } completion:nil];
        
        
    }
}
@end
