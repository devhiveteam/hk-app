//
//  main.m
//  HappyKing
//
//  Created by Leonardo Cid on 28/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PListData.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        [PListData initialize:@"themes.plist"];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
