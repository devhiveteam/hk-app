//
//  Definitions.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/13/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#ifndef Definitions_h
#define Definitions_h
#define USE_UBERTESTERS 0

#define kMainFontWithSize(fontSize) [UIFont fontWithName:@"QuasixTitling-Regular" size:fontSize]


typedef enum {
    TypeQuick,
    TypeSingle,
    TypeMultiplayer
}GameType;


#endif /* Definitions_h */
