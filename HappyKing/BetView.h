//
//  BetView.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/12/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BetView;

@protocol BetViewDelegate <NSObject, UITextFieldDelegate>
- (void)betView:(BetView*)view didMakeBet:(NSNumber*)coins;
@end

@interface BetView : UIView {
    int currentCoins;
    int betCoins;
}
@property (nonatomic, strong) id<BetViewDelegate> delegate;

+ (BetView*)betView;
- (void)showInView:(UIView*)view;
- (void)dismiss;

@end
