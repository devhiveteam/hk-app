//
//  InAppPurchasesHelper.h
//  HappyKing
//
//  Created by Leonardo Cid on 03/07/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

#define IAPHelperProductPurchasedNotification @"IAPHelperProductPurchasedNotification"

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface InAppPurchasesHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>


+ (InAppPurchasesHelper *)sharedInstance;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(SKProduct *)product;
@end
