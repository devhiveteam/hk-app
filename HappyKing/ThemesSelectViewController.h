//
//  ThemesStoreViewController.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/16/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol themeChanged
- (void)delegateEventThemeChanged;
@end

@interface ThemesSelectViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    id delegate;
    
    CGSize cellSize;
    NSArray *availableThemesArray;
    NSString *typeTheme;
    NSString *nameObjectTheme;
    NSString *nameElement;
}

@property (nonatomic, strong)IBOutlet UIImageView *backgroundThemeSelectImageView;

- (void)setType: (NSString *)type withObjectName: (NSString *) objectName;
- (void)setDelegate: (id)newDelegate;

@end
