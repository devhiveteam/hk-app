//
//  ThemesStoreViewController.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/16/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "ThemesSelectViewController.h"
#import "PListData.h"

@interface ThemesSelectViewController()
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIButton *saveThemeButton;

@end

@implementation ThemesSelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *ImageProfileTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageProfileTheme != nil)
    {
        [self.backgroundThemeSelectImageView setImage : [UIImage imageNamed:ImageProfileTheme]];
        //[self.backgroundThemeSelectImageView setContentMode: UIViewContentModeScaleAspectFit];
        //[self.backgroundThemeSelectImageView setContentMode:UIViewContentModeCenter];
    }

    availableThemesArray = [PListData getItemData:typeTheme fromFile:@"themes"]; //cargar desde plist guardado localmente
    CGRect rect = self.view.frame;
    CGSize size = CGSizeZero;
    size.height = (rect.size.height - 10 - 65) / 2;
    size.width = (rect.size.width - 60) / 5;
    cellSize = size;
}

- (void)setType:(NSString *)type withObjectName: (NSString *) objectName {
    typeTheme = type;
    nameObjectTheme = objectName;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return availableThemesArray.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ThemeCell" forIndexPath:indexPath];
    
    UIImageView *imgView = (UIImageView*)[cell viewWithTag:1];
    UILabel *nameLabel =  (UILabel*)[cell viewWithTag:2];
    
    NSDictionary *dict = [availableThemesArray objectAtIndex:indexPath.row];
    BOOL isActive = [dict[@"active"] boolValue];
    BOOL isUnlock = ([typeTheme isEqualToString:@"Cards"] || [typeTheme isEqualToString:@"Tables"]) ? [dict[@"unlocked"] boolValue] : true;

    imgView.image = [UIImage imageNamed:dict[nameObjectTheme]];
    
    
    NSString *price = dict[@"price"];
    
    if ([typeTheme isEqualToString:@"Cards"]) {
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
        [formatter setGroupingSeparator:groupingSeparator];
        [formatter setGroupingSize:3];
        [formatter setAlwaysShowsDecimalSeparator:NO];
        [formatter setUsesGroupingSeparator:YES];
        [formatter setMaximumFractionDigits:0];
        
        NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithInteger: [dict[@"price"] integerValue]]];
        NSString *firstChar = [formattedString substringToIndex:1];
        NSString *coinChar = @"$ ";
        if([firstChar isEqualToString:@"$"])
            coinChar = @"";
        
        price = [NSString stringWithFormat:@"HK%@%@", coinChar, formattedString];
    }
    
    if ([typeTheme isEqualToString:@"Tables"]) {
        price = [NSString stringWithFormat:@"%@ juegos", dict[@"price"]];
    }
    
    nameLabel.text = isUnlock ? dict[@"name"] : price;
    
    if(!isUnlock)
    {
        [cell setUserInteractionEnabled:NO];
        [cell setAlpha:0.5];
    }

    
    if (isActive) {
        [cell setSelected:YES];
        cell.backgroundColor = [UIColor blueColor];
        //nameElement = nameLabel.text;
        
        [self.collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPath];
    }
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return cellSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor blueColor];
    
    UILabel *nameLabel =  (UILabel*)[cell viewWithTag:2];
    nameElement = [nameLabel.text copy];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell =[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = nil;
}

- (IBAction)exitButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)setDelegate: (id)newDelegate{
    delegate = newDelegate;
}

- (IBAction)saveThemeButtonPressed:(id)sender {
    if(nameElement != nil){
        [PListData saveActiveItemForKey:typeTheme withName:nameElement fromFile:@"themes"];
        [delegate delegateEventThemeChanged];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
