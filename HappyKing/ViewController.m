//
//  ViewController.m
//  HappyKing
//
//  Created by Leonardo Cid on 28/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "ViewController.h"

#import "NSMutableArray+Shuffling.h"
#import "GCHelper.h"
#import "PListData.h"
#import "Definitions.h"
#import "ScoreView.h"
#import "UIView+Stuff.h"
#import "L360ConfettiArea.h"

#import <IQKeyboardManager.h>

#define NUMBER_OF_PLAYERS 4
#define MESSAGE_HAPPY_KING @"¡REY CONTENTO!"
#define MESSAGE_FRIENDLY_PAIR @"¡PAR AMIGO!"
#define MESSAGE_DAMNED_THRICE @"¡OH NO,\n TERCIA MALDITA!\n-1 VIDA!"
#define MESSAGE_FOUR_OF_A_KIND @"¡POKER!\n+1 VIDA"
#define MESSAGE_QUEEN @"¡DAMA!"
#define MESSAGE_PLAYER_OUT @"¡%@ HA QUEDADO FUERA!"
#define MESSAGE_WINNER @"¡%@ HA GANADO!"
#define MESSAGE_PICTURE @"¡¡PICTURE!!"
#define MESSAGE_MINUS_PLUS_ONE @"¡¡-1/+1!!"

#define CONTENT_LABEL_FONT_SIZE 14
#define TITLE_LABEL_FONT_SIZE 13

#define kAlertViewAfterQuickGameTag 1
#define kAlertViewExitGameTag 2
#define kAlertViewPlayerLostKeepTag 3

#define kMessageTypeKey @"HKMessageType"

typedef void (^ShowMessageCompletionhandler)(void);

@import Foundation;

@interface ViewController ()
@property (nonatomic, weak) IBOutlet UIButton *switchCardButton;
@property (nonatomic, weak) IBOutlet UIButton *keepCardButton;
@property (nonatomic, weak) IBOutlet UIButton *nextRoundButton;
@property (nonatomic, weak) IBOutlet UIButton *backRoundButton;

@property (nonatomic, weak) IBOutlet UILabel *lblCurrentCoins;
@property (nonatomic, weak) IBOutlet UILabel *lblCurrentCoinsValue;
@property (nonatomic, weak) IBOutlet UILabel *lblBetCoins;
@property (nonatomic, weak) IBOutlet UILabel *lblBetCoinsValue;
@property (nonatomic, weak) IBOutlet UIView *betView;

@property (nonatomic, strong) NSMutableArray *deck;
@property (nonatomic, strong) NSMutableArray *players;
@property (nonatomic, strong) NSMutableArray *positionsArray;
@property (nonatomic, strong) Player *currentPlayer;
@property (nonatomic, strong) Player *userPlayer;
@property (nonatomic, strong) Player *dealer;
@property (nonatomic, strong) UIView *deckView;
@property (nonatomic) BOOL areAllCardFlipped;
@property (nonatomic) GameState gameState;
@property (nonatomic) BOOL cardsShown;
@property (nonatomic) NSMutableIndexSet *koPlayers;
@property (nonatomic, strong) NSMutableArray *finalRoundMessages;
@property (nonatomic, strong) UIFont *buttonsFont;
@property (nonatomic, strong) UIFont *gameFont;
@property (nonatomic) NSUInteger wagerCoins;
@property (nonatomic) NSArray<GKPlayer*> *playersMP;
@property (nonatomic) NSString *serverIDMP;
@property (nonatomic) int offsetMP;

@property (nonatomic) BOOL amIServer;

@property (nonatomic, strong) L360ConfettiArea *confettiArea;

- (NSArray *)colorsForConfettiArea:(L360ConfettiArea *)confettiArea;
- (void)conffettize:(NSTimer*)timer;
- (void)conffettize2:(NSTimer*)timer;
- (void)conffettize3:(NSTimer*)timer;
- (BOOL)localPlayerWillBeServer;
- (void)numberPlayersMP;
- (void)announceDealerMP;
- (void)dispatchMessageMP:(NSDictionary*)message;
- (void)initializePlayersMP:(NSDictionary*)data;
- (void)sendGameStateMP:(GameState)state;
- (void)dealCardToPlayerMP:(NSString*)playerID card:(Card*)card;

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view insertSubview:imageView atIndex:0];
    [imageView  setBackgroundColor:[UIColor clearColor]];
    [imageView setImage:[UIImage imageNamed:@"HK_Mesa01_Fondo_B"]];
    
    imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    [imageView  setBackgroundColor:[UIColor clearColor]];
    [imageView setImage:[UIImage imageNamed:@"BoardCurve"]];
    [self.view insertSubview:imageView atIndex:1];
    
//    [self.view setBackgroundColor:[UIColor colorWithRed:0.09 green:0.24 blue:0.1 alpha:1.0]];

//    CGRect rect = [[UIScreen mainScreen] bounds];
//    UIView *view = [[UIView alloc] initWithFrame:(CGRect){{50,50},{rect.size.width - 100, rect.size.height - 100}}];
//    view.layer.cornerRadius = 50;
//    view.layer.borderWidth = 2;
//    view.layer.borderColor = [UIColor colorWithRed:0.82 green:0.89 blue:0.0 alpha:1.0].CGColor;
//    [self.view insertSubview:view atIndex:1];
    
    self.deckView = [[UIView alloc] initWithFrame:(CGRect){{0,0},{60,80}}];
//    [self.deckView setHidden:YES];
    [self.view addSubview:self.deckView];
    
    if (self.gameType != TypeMultiplayer) {
        self.deck = [self createDeck];
    }
    
    UIImage *image = [UIImage imageNamed:@"Mesa01_Boton_Activo"];
//    UIImage *image1 = [UIImage imageNamed:@"A1"];
//    UIImage *image2 = [UIImage imageNamed:@"A2"];
//    UIImage *image3 = [UIImage imageNamed:@"A3"];
//    UIImage *image4 = [UIImage imageNamed:@"A4"];
    NSString *ImageButtonTheme = [PListData getValueWithKey:@"button" fromElement:@"Buttons" fromFile:@"themes"];
    if(ImageButtonTheme != nil)
    {
        image = [UIImage imageNamed:ImageButtonTheme];
    }
    
    //UIImage *imageDisabled = [UIImage imageNamed:@"Mesa01_Boton_Inactivo"];
//    UIImage *imageDisabled1 = [UIImage imageNamed:@"N1"];
//    UIImage *imageDisabled2 = [UIImage imageNamed:@"N2"];
//    UIImage *imageDisabled3 = [UIImage imageNamed:@"N3"];
//    UIImage *imageDisabled4 = [UIImage imageNamed:@"N4"];
    
    self.buttonsFont = kMainFontWithSize(12);
    self.gameFont = kMainFontWithSize(30);
    
    self.finalRoundMessages = [NSMutableArray array];
    self.koPlayers = [NSMutableIndexSet indexSet];
    [self.switchCardButton setBackgroundImage:image forState:UIControlStateNormal];
//    [self.switchCardButton setBackgroundImage:imageDisabled2 forState:UIControlStateDisabled];
    [self.switchCardButton.titleLabel setFont:_buttonsFont];
    
    [self.keepCardButton setBackgroundImage:image forState:UIControlStateNormal];
//    [self.keepCardButton setBackgroundImage:imageDisabled3 forState:UIControlStateDisabled];
    [self.keepCardButton.titleLabel setFont:_buttonsFont];
    
    [self.nextRoundButton setBackgroundImage:image forState:UIControlStateNormal];
//    [self.nextRoundButton setBackgroundImage:imageDisabled1 forState:UIControlStateDisabled];
    [self.nextRoundButton.titleLabel setFont:_buttonsFont];
    
    [self.backRoundButton setBackgroundImage:image forState:UIControlStateNormal];
//    [self.backRoundButton setBackgroundImage:imageDisabled4 forState:UIControlStateDisabled];
    [self.backRoundButton.titleLabel setFont:_buttonsFont];
    
    [self prepareGame];
    
    achievementsManager = [AchievementsManager sharedManager];
    
    [[GCHelper sharedManager] addObserver:self forKeyPath:@"coins" options:NSKeyValueObservingOptionNew context:nil];
    [[GCHelper sharedManager] addObserver:self forKeyPath:@"diamonds" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [[ScoreView sharedInstance] showInView:self.view];
//    [self dealCardToCurrentPlayer];

}

- (void)dealloc {
    [[GCHelper sharedManager] removeObserver:self forKeyPath:@"coins"];
    [[GCHelper sharedManager] removeObserver:self forKeyPath:@"diamonds"];
    if ([timer1 isValid]) {
        [timer1 invalidate];
    }
    [self.confettiArea removeFromSuperview];
    self.confettiArea = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.gameType == TypeMultiplayer) {
        self.match = [[GCHelper sharedManager] currentMatch];
        self.gameState = Initializing;
        [self setupPlayersPositions];
        [self addCardsToScreen:self.deck initialPositionValue:0];
        NSLog(@"n of players: %d", (int)self.match.players.count);
        /*[self.match chooseBestHostingPlayerWithCompletionHandler:^(GKPlayer * _Nullable player) {
            GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
            NSLog(@"Local player: %@", localPlayer.playerID);
            NSLog(@"Best host: %@", player.playerID);
            if([localPlayer.playerID isEqualToString:player.playerID]){
                NSLog(@"I am the chosen one");
                self.amIServer = [self localPlayerWillBeServer];
                if(self.amIServer) {
                    _playersMP = [[NSMutableArray<GKPlayer*> alloc] initWithCapacity:self.match.players.count];
                    
                    [(NSMutableArray<GKPlayer*> *)_playersMP addObject:localPlayer];
                    for (int i = 0; i < self.match.players.count; i++) {
                        [(NSMutableArray<GKPlayer*> *)_playersMP addObject:self.match.players[i]];
                    }
                    
                    [self numberPlayersMP];
                    [self createPlayers];
                    [self positionDeck];
                }
            } else {
                if (player == Nil) {*/
                    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
                    NSMutableArray<GKPlayer*> *names = [[NSMutableArray<GKPlayer *> alloc] initWithCapacity:(_match.players.count + 1)];
            
                    [names addObject:localPlayer];
                    
                    for (int i = 0; i < _match.players.count; i++) {
                        [names addObject:_match.players[i]];
                    }
                    
                    NSLog(@"Names: %@", names);
                    
                    NSArray<GKPlayer*> *sortedNames = [names sortedArrayUsingComparator:^NSComparisonResult(GKPlayer*  _Nonnull player1, GKPlayer*  _Nonnull player2) {
                        return [player1.playerID compare:player2.playerID];
                    }];
                    
                    NSLog(@"Sorted Names: %@", sortedNames);
            
                    if([localPlayer.playerID isEqualToString:sortedNames[0].playerID]) {
            
                        NSLog(@"I am the chosen one");
                        self.amIServer = [self localPlayerWillBeServer];
                        if(self.amIServer) {
                            _playersMP = sortedNames;
                            
                            self.deck = [self createDeck];
                            [self sendDeckToOtherPlayers:self.deck];
                            [self addCardsToScreen:self.deck initialPositionValue:0];
                            [self numberPlayersMP];
                            [self createPlayers];
                            [self positionDeck];
                            [self dealInitialCards];
                        }
                    } else {
                        self.amIServer = FALSE;
                    }

                /*} else {
                    self.amIServer = FALSE;
                }
            }
        }];*/
        
    } else if (self.gameType == TypeSingle){
        self.gameState = Initializing;
        [self setupPlayersPositions];
        [self addCardsToScreen:self.deck initialPositionValue:0];
        [self createPlayers];
        [self positionDeck];
        [self dealInitialCards];
    } else {
        [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
        self.gameState = Initializing;
        [self showBetScreen];
    }
    
}

- (NSArray *)colorsForConfettiArea:(L360ConfettiArea *)confettiArea
{
    return @[[UIColor orangeColor], [UIColor yellowColor], [UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initializing methods

- (void)prepareGame {
    switch(self.gameType) {
        case TypeSingle: {
            self.betView.hidden = NO;
            self.lblCurrentCoins.font = kMainFontWithSize(TITLE_LABEL_FONT_SIZE);
            self.lblCurrentCoinsValue.font = kMainFontWithSize(CONTENT_LABEL_FONT_SIZE);
            self.lblBetCoins.font = kMainFontWithSize(TITLE_LABEL_FONT_SIZE);
            self.lblBetCoinsValue.font = kMainFontWithSize(CONTENT_LABEL_FONT_SIZE);
            
            self.wagerCoins = [self getTotalWagerCoins:self.numberOfPlayer withUsersWager:true];
        }
            break;
        case TypeMultiplayer:
        {
            self.betView.hidden = NO;
            self.lblCurrentCoins.font = kMainFontWithSize(TITLE_LABEL_FONT_SIZE);
            self.lblCurrentCoinsValue.font = kMainFontWithSize(CONTENT_LABEL_FONT_SIZE);
            self.lblBetCoins.font = kMainFontWithSize(TITLE_LABEL_FONT_SIZE);
            self.lblBetCoinsValue.font = kMainFontWithSize(CONTENT_LABEL_FONT_SIZE);
            
            self.wagerCoins = [self getTotalWagerCoins:self.numberOfPlayer withUsersWager:true];
        }
            break;
        case TypeQuick:
        {
            self.betView.hidden = NO;
            self.lblCurrentCoins.font = kMainFontWithSize(16);
            self.lblCurrentCoinsValue.font = kMainFontWithSize(20);
            self.lblBetCoins.font = kMainFontWithSize(16);
            self.lblBetCoinsValue.font = kMainFontWithSize(20);
            
        }
            break;
    }
}

- (NSMutableArray *)createDeck {
    NSMutableArray *deck = [NSMutableArray array];
    for (Suit suit = Spades; suit <= Diamonds; suit++ ) {
        for (Rank rank = Ace; rank <= King; rank++) {
            Card *card = [Card createCardWithSuit:suit andRank:rank];
            CardView *cardView = [CardView cardViewWithCard:card];
            [deck addObject:cardView];
        }
    }
    [deck shuffle];
    return deck;
}

- (void)positionDeck {
    NSInteger dealerIndex = [self.players indexOfObject:self.dealer];
    CGPoint position = [_positionsArray[dealerIndex] CGPointValue];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    if (position.x == kPlayerBoardMargin) {
        position.x += (kPlayerBoardViewWidth + kPlayerBoardMargin);
    } else if (position.x == screenSize.width - kPlayerBoardViewWidth - kPlayerBoardMargin) {
        position.x -= (_deckView.frame.size.width + kPlayerBoardMargin);
    } else {
        position.x += (kPlayerBoardViewWidth + kPlayerBoardMargin);
    }
    
    CGRect frame = self.deckView.frame;
    frame.origin = position;
    self.deckView.frame = frame;
    
    CGPoint center = self.deckView.center;
    center.y = position.y + kPlayerBoardViewHeight / 2;
    self.deckView.center = center;
    
}

- (void)createPlayers {
    self.players = [NSMutableArray array];
    if (self.gameType == TypeMultiplayer) {
        for (int i = 0; i < _numberOfPlayer; i++) {
            Player *player = [[Player alloc] initWithContainerview:self.view withGameType:self.gameType];
            player.delegate = self;
            
            player.gcPlayer = self.playersMP[i];
            player.playerName = player.gcPlayer.alias;
                
            [self.players addObject:player];
            [player setPosition:[_positionsArray[i] CGPointValue]];
            if (i > 0) {
                Player *lastPlayer = self.players[i - 1];
                player.leftPlayerIndex = i - 1;
                player.rightPlayerIndex = i + 1;
                player.previousPlayer = lastPlayer;
                lastPlayer.nextPlayer = player;
            }
            if (i == _numberOfPlayer - 1) {
                Player *nextPlayer = self.players[0];
                nextPlayer.previousPlayer = player;
                player.nextPlayer = nextPlayer;
                player.rightPlayerIndex = 0;
            }
            
        }
    } else {
        for (int i = 0; i < _numberOfPlayer; i++) {
            Player *player = [[Player alloc] initWithContainerview:self.view withGameType:self.gameType];
            player.delegate = self;
            if (self.match) {
                player.gcPlayer = self.match.players[i];
            } else {
                NSString *string = [NSString stringWithFormat:@"M%d", i];
                player.playerName = string;
            }
            [self.players addObject:player];
            [player setPosition:[_positionsArray[i] CGPointValue]];
            if (i > 0) {
                Player *lastPlayer = self.players[i - 1];
                player.leftPlayerIndex = i - 1;
                player.rightPlayerIndex = i + 1;
                player.previousPlayer = lastPlayer;
                lastPlayer.nextPlayer = player;
            }
            if (i == _numberOfPlayer - 1) {
                Player *nextPlayer = self.players[0];
                nextPlayer.previousPlayer = player;
                player.nextPlayer = nextPlayer;
                player.rightPlayerIndex = 0;
            }

        }
    }
    
    int firstPlayerIndex = 0;
    int playerIndex = -1;
    if (self.match) {
//        firstPlayerIndex = [self.match.players indexOfObject:self.match.currentParticipant]; //Leo cid
//        for (int i = 0; i < self.match.participants.count; i++) {
//            GKTurnBasedParticipant *participant = self.match.participants[i];
//            if ([participant.player.playerID isEqualToString:[[GKLocalPlayer localPlayer] playerID]]) {
//                playerIndex = i;
//            }
//        }
    }
    Player *firstPlayer = self.players[firstPlayerIndex];
    Player *lastPlayer = self.players[_numberOfPlayer - 1];
    Player *userPlayer = self.players[0];
    if (playerIndex != -1) {
        userPlayer = self.players[playerIndex];
    }
    firstPlayer.nextPlayer = self.players[1];
    firstPlayer.previousPlayer = lastPlayer;
    firstPlayer.rightPlayerIndex = 1;
    firstPlayer.leftPlayerIndex = _numberOfPlayer - 1;
    self.userPlayer = userPlayer;
    NSInteger dealerIndex = arc4random_uniform(_numberOfPlayer);
    self.dealer = self.players[dealerIndex];
    if (self.gameType == TypeMultiplayer && self.amIServer){
        self.dealer = self.players[0];
        [self announceDealerMP];
    }
    self.userPlayer.isUser = YES;
    self.currentPlayer = self.dealer.nextPlayer;
}

- (void)setupPlayersPositions {
    _positionsArray = [NSMutableArray array];
    CGSize size = [[UIScreen mainScreen] bounds].size;
    switch(_numberOfPlayer) {
        case 2: {
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth / 2, kPlayerBoardMargin}]];
        }
        case 3:
        case 4:
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth / 2, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
        
            break;
        case 5: {
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
        }
            break;
        case 6: {
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth / 2, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
        }
            break;
        case 7: {
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
        }
            break;
        case 8: {
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth /2 , size.height - kPlayerBoardViewHeight - kPlayerBoardMargin - kPlayerUserBottomSpace}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width - kPlayerBoardViewWidth - kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){size.width / 2 - kPlayerBoardViewWidth / 2, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, kPlayerBoardMargin}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height / 2 - kPlayerBoardViewHeight / 2}]];
            [_positionsArray addObject:[NSValue valueWithCGPoint:(CGPoint){kPlayerBoardMargin, size.height - kPlayerBoardViewHeight - kPlayerBoardMargin}]];
        }
            break;
    }
    
}

- (void)addCardsToScreen:(NSMutableArray*)deck initialPositionValue:(int)position{
    int pos = position;
    for (NSInteger i = 0; i < deck.count; i++, pos++) {
        int currentX = 0;
        int currentY = 0;
        CardView *cardView = deck[i];
        CGRect rect = cardView.frame;
        currentX += (pos%5) * 5;
        currentY += (pos%5) * 5;
        rect.origin = (CGPoint){currentX, currentY};
        cardView.frame = rect;
        [self.deckView insertSubview:cardView atIndex:0];
    }
}

#pragma mark - Game State machine

- (void)executeNextMove:(NSTimer*)timer {
    BOOL restartTimer = NO;
    CGFloat nextTimer = 1.3f;
    BOOL picture = NO;
    switch(_gameState) {
        case InitialDealing:
        {
            [self dealCardToCurrentPlayer];
            BOOL wasLastPlayerDealer = self.currentPlayer.isDealer;
            if (wasLastPlayerDealer) {
                if (self.currentPlayer.nextPlayer.isUser) {
                    self.gameState = UserDeciding;
                } else {
                    self.gameState = MachineDeciding;
                    restartTimer = YES;
                }
                if (HAS_RESULT_VALUE([self evaluateHappyKing], ResultKing)) {
                    self.gameState = FinishedRound;
                    restartTimer = YES;
                }
            } else {
                restartTimer = YES;
                nextTimer = CARD_DEAL_TIMER;
            }
            [self moveToNextPlayer];
        }
            break;
        case MachineDeciding:
        {
            if (!_currentPlayer.isDealer) {
                
                BOOL canCardFromNextPlayerBeSwapped = [_currentPlayer.nextPlayer canCardBeSwapped];
                if (_currentPlayer.shouldChange  && canCardFromNextPlayerBeSwapped ) {
                    Player *nextPlayer = _currentPlayer.nextPlayer;
                    picture = [_currentPlayer switchCardWithPlayer:nextPlayer];
                    [self showPicture:picture];
                    _currentPlayer.changedCard = YES;
                } else if (_currentPlayer.cardView.card.rank <= 3 && canCardFromNextPlayerBeSwapped) {
                    Player *nextPlayer = _currentPlayer.nextPlayer;
                    picture = [_currentPlayer switchCardWithPlayer:nextPlayer];
                    [self showPicture:picture];
                    _currentPlayer.changedCard = YES;
                } else if (_currentPlayer.cardView.card.rank <=  7 && canCardFromNextPlayerBeSwapped) { //if it's the first dealing and the card is below 6 we make a random decision to switch cards.
                    BOOL changeCard = arc4random_uniform(5) <= 2;
                    if (changeCard) {
                        picture = [_currentPlayer switchCardWithPlayer:_currentPlayer.nextPlayer];
                        [self showPicture:picture];
                    }
                    _currentPlayer.changedCard = changeCard;
                } else if (_currentPlayer.cardView.card.rank == Queen) {
                    Player *previousPlayer = _currentPlayer.previousPlayer;
                    BOOL changeCard = arc4random_uniform(5) < 2;
//                    if (previousPlayer.changedCard && changeCard) {
//                        picture = [previousPlayer switchCardWithPlayer:previousPlayer.previousPlayer];
//                        [self showPicture:picture];
//                        previousPlayer.changedCard = NO;
//                    }
                }
                [self moveToNextPlayer];
                restartTimer = YES;
                if (self.currentPlayer.isUser) {
                    self.gameState = UserDeciding;
                    restartTimer = NO;
                }
                
            } else {
                if (_currentPlayer.shouldChange) {
                    [self dealerKillMove:NO];
                } else if (_currentPlayer.cardView.card.rank <= 3) {
                    [self dealerKillMove:NO];
                } else if (_currentPlayer.cardView.card.rank <= 7) { //if it's the first dealing and the card is below 6 we make a random decision to switch cards.
                    BOOL changeCard = arc4random_uniform(5) < 2;
                    if (changeCard) {
                        [self dealerKillMove:NO];
                    }
                    _currentPlayer.changedCard = changeCard;
                } else if (_currentPlayer.cardView.card.rank == Queen) {
                    Player *previousPlayer = _currentPlayer.previousPlayer;
                    BOOL changeCard = arc4random_uniform(5) < 2;
//                    if (previousPlayer.changedCard && changeCard) {
//                        picture = [previousPlayer switchCardWithPlayer:previousPlayer.previousPlayer];
//                        [self showPicture:picture];
//                        previousPlayer.changedCard = NO;
//                    }
                }
                _currentPlayer.isMyTurn = NO;
                self.gameState = FinishedRound;
                restartTimer = YES;
            }
        }
            break;
        case FinishedRound: {
            if (!self.areAllCardFlipped) {
                [self.players makeObjectsPerformSelector:@selector(revealCard)];
                self.areAllCardFlipped = YES;
                restartTimer = YES;
                nextTimer = 1.0;
            } else {
                [achievementsManager updateTotalRoundsPlayed:achievementsManager.totalRoundsPlayed + 1 withHandler:^(GKAchievement *achievement, NSError *error) {
                    NSLog(@"Error: %@", [error localizedDescription]);
                }];

                NSMutableDictionary *occurrencesByRank = [[self getNumberOfOcurrencesForEachRank] mutableCopy];
                if ([occurrencesByRank[@(King)] count] != 0) { //there is at least one King
                    [occurrencesByRank[@(King)] enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                        Player *player = self.players[idx];
                        player.finalState = StateHasKing;
                    }];
                    [self enqueueFinalStateMessage:MessageKing withObject:nil];
                }
                
                if ([occurrencesByRank[@(King)] count] == 1)
                    [occurrencesByRank removeObjectForKey:@(King)];
                
                
                NSArray *keys = [occurrencesByRank allKeys];
//                NSInteger numberOfPairs = 0;
//                NSMutableArray *pairRanks = [NSMutableArray array];
//                for (id key in keys) {
//                    NSIndexSet *set = occurrencesByRank[key];
//                    __block NSInteger repetitions = [set count];
//                    FinalState finalState = (FinalState)repetitions;
//                    if (finalState == StateSamePair) {
//                        numberOfPairs++;
//                        [pairRanks addObject:key];
//                    }
//                    
//                }
//                NSInteger numberOfActivePlayers = [self getPlayersAvailable:false];
//                if (numberOfPairs * 2 == numberOfActivePlayers - 1) { //one will lose regardless of their rank
//                    for (NSNumber *number in pairRanks) {
//                        for (Player *player in self.players) {
//                            Player *player = 
//                        }
//                    }
//                    
//                }
                
                
                __block BOOL allSaved = NO;
                for (id key in keys) {
                    NSIndexSet *set = occurrencesByRank[key];
                    __block NSInteger repetitions = [set count];
                    [set enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                        FinalState finalState = (FinalState)repetitions;
                        if (finalState == StateSamePair) {
                            [self enqueueFinalStateMessage:MessagePair withObject:nil];
                        } else if (finalState == StateSameThreeOfAKind) {
                            [self enqueueFinalStateMessage:MessageDamnedThrice withObject:nil];
                        } else if (finalState == StateSameFourOfAKind) {
                            [self enqueueFinalStateMessage:MessageFourOfAKind withObject:nil];
                        }
                        Player *player = self.players[idx];
                        player.finalState = (FinalState)repetitions;
                        allSaved = player.finalState == StateSameThreeOfAKind; //Tercia maldita se salvan todos los demás.
                        
                        Player *userPlayer = self.userPlayer;
                        if (finalState == StateSameThreeOfAKind) {
                            if (userPlayer.finalState == StateSameThreeOfAKind) {
                                [achievementsManager userWonThreeOfAKind:^(GKAchievement *achievement, NSError *error) {
                                    
                                }];
                            }
                        } else if (finalState == StateSamePair) {
                            if (userPlayer.finalState == StateSamePair) {
                                [achievementsManager friendlyPair:^(GKAchievement *achievement, NSError *error) {
                                    
                                }];
                            }
                        }
                    }];
                }
                
                Player *player = [self getLowestRankCard: [occurrencesByRank allKeys]];
                if (player.finalState == StateHasKing) {
                    player.finalState = StateUndefined;
                    [self.finalRoundMessages removeObject:@{@"type": @(King)}];
                }
                if (player.finalState == StateUndefined && !allSaved) {
                    if ([self getPlayersAvailable:false] == 2
                        && [self evaluateForPlusMinusOne]) {
                        [self.players makeObjectsPerformSelector:@selector(incrementOneLife)];
                        [self.koPlayers removeAllIndexes];
                        [self enqueueFinalStateMessage:MessageMinusPlusOne withObject:nil];
                    } else {
                        player.finalState = StateLowest;
                        if(player == self.userPlayer && player.cardView.card.rank == King) {
                            [achievementsManager userLostWithKing:^(GKAchievement *achievement, NSError *error) {
                                
                            }];
                        }
                    }
                }
                if (self.userPlayer.finalState != StateLowest && self.userPlayer.finalState != StateOut && self.userPlayer.finalState != StateUndefined) {
                    if (self.gameType != TypeQuick) {
                        /*NSUInteger coins = [GCHelper sharedManager].coins;
                        coins += self.userPlayer.cardView.card.rank;
                        [[GCHelper sharedManager] setCoins:coins];
                        [achievementsManager updateTotalCoinsWon:achievementsManager.totalCoinsWon + coins withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                            
                        }];*/
                        if ([[GCHelper sharedManager] gameCenterEnabled]) {
                            NSUInteger score = [GCHelper sharedManager].mainLeaderboardScore;
                            score += self.userPlayer.cardView.card.rank;
                            [[GCHelper sharedManager] updateScoreAndSend:score];
                        }
                    }
                    
                    
                    if (occurrencesByRank[@(Queen)] != 0) {
                        [achievementsManager userKilledQueen:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    }
                    if (self.userPlayer.cardView.card.rank == Seven) {
                        [achievementsManager userWonWith7:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    } else if (self.userPlayer.cardView.card.rank == Two) {
                        [achievementsManager userWonWith2:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    }
                }
                [self showFinalMessages:^{
                    if ([self getPlayersAvailable:NO] == 1) {
                        Player *winnerPlayer = nil;
                        for (Player *player in self.players) {
                            if (player.lives != 0) {
                                winnerPlayer = player;
                                winnerPlayer.finalState = StateWinner;
                            }
                        }
                        self.gameState = GameFinished;
                        [self restartTimerWithInterval:nextTimer];
                    } else {
                        if (self.userPlayer.finalState == StateOut && [self getPlayersAvailable:NO] >= 2 && !hasStayInGameMessageBeenShown) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Rey Contento" message:@"¿Deseas continuar en el juego o salir al menú principal?" delegate:self cancelButtonTitle:@"Salir" otherButtonTitles:@"Continuar", nil];
                            alertView.tag = kAlertViewPlayerLostKeepTag;
                            [alertView show];
                            hasStayInGameMessageBeenShown = YES;
                        } else {
                            if (self.gameType == TypeSingle || self.gameType == TypeQuick) {
                                self.gameState = BetweenRounds;
                            }
                        }
                        if (self.gameType == TypeMultiplayer)
                            [self.nextRoundButton setEnabled: YES];
                    }
                }];
                
            }
//            int result = [self evaluateHappyKing];
//            if (HAS_RESULT_VALUE(result, ResultKing)) {
//                NSIndexSet *indexesForKings = [self indexesWithRank:King];
//            }
            
//            Player *winnerPlayer = self.players[winnerPlayerIndex];
//            winnerPlayer.finalState = StateHasKing;
        }
            break;
        case BetweenRounds:
            [self showMessage:[NSString stringWithFormat:@"Siguiente ronda inicia en:\n%d", secondsToNextRound] andColor: [UIColor yellowColor] withCompletion:^{
                
            }];
            secondsToNextRound--;
            if (secondsToNextRound == 0) {
                if ([self readyGameForNextRound]) {
                    self.gameState = InitialDealing;
                    [self restartTimerWithInterval:2.0];
                } else { //Not enough cards to play
                    
                }
            } else {
                [self restartTimerWithInterval:nextTimer];
            }
            break;
        case GameFinished:
        {
            Player *winner = [self getWinner];
            
            if (self.gameType == TypeQuick) {
                if (winner == self.userPlayer) {
                    
                    [[GCHelper sharedManager] incrementCoins: self.wagerCoins];
                    
                    [achievementsManager updateTotalCoinsWon:(achievementsManager.totalCoinsWon + self.wagerCoins) withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                    
                    }];
                }  else {
                    [[GCHelper sharedManager] incrementCoins:- self.wagerCoins];
                }
            } else {
                if (winner == self.userPlayer) {
                    long coinsWon = [self getTotalWagerCoins:self.numberOfPlayer withUsersWager:FALSE];
                    [[GCHelper sharedManager] incrementCoins: coinsWon];
                    [self animateCoinsWinner];
                    [achievementsManager updateTotalCoinsWon:(achievementsManager.totalCoinsWon + coinsWon) withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                        
                    }];
                }  else {
                    [[GCHelper sharedManager] incrementCoins:- [self getIndividualEntryWager:self.numberOfPlayer]];
                }
            }
            [self showMessageWithType:MessageWinner withObject:winner.playerName andCompletion:^{
                if (winner == self.userPlayer) {
                    [self createConffettiAndAdddToView];
                    
//
                    timer1 = [NSTimer scheduledTimerWithTimeInterval: 1.5
                                                             target: self
                                                           selector: @selector(conffettize:)
                                                           userInfo: nil
                                                            repeats: YES];
                    
//                    timer2 = [NSTimer scheduledTimerWithTimeInterval: 1.5
//                                                              target: self
//                                                            selector: @selector(conffettize2:)
//                                                            userInfo: nil
//                                                             repeats: YES];
//                    
//                    timer3 = [NSTimer scheduledTimerWithTimeInterval: 1.5
//                                                              target: self
//                                                            selector: @selector(conffettize3:)
//                                                            userInfo: nil
//                                                             repeats: YES];
                }
                
//                if (self.gameType == TypeQuick)
                {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self showRestartGame];
                    });
                }
    
            }];
        }
            break;
        default:
            break;
            
    }
    
    if (restartTimer) {
        [self restartTimerWithInterval:nextTimer];
    } else {
        if (self.gameState == UserDeciding) {
            [self.userPlayer startTimerForTurn];
        }
    }
}

- (void)showRestartGame {
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Rey Contento"
                                                     message:@"¿Iniciar de nuevo?"
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles: nil];
    [alert addButtonWithTitle:@"Sí"];
    alert.tag = kAlertViewAfterQuickGameTag;
    [alert show];
}

- (void)conffettize:(NSTimer*)timer{
    CGSize size = self.view.bounds.size;
    int minX = size.width / 4;
    int minY = size.height / 4;
    int maxX = (size.width / 4) * 3;
    int maxY = (size.height / 4) * 3;
    int x = (arc4random() % (maxX - minX)) + minX;
    int y = (arc4random() % (maxY - minY)) + minY;
    [self.confettiArea burstAt:CGPointMake(x, y) confettiWidth:5.0 numberOfConfetti:10];
}

- (void)conffettize2:(NSTimer*)timer{
    CGSize size = self.view.bounds.size;
    int x = (size.height / 4) * 3;
    int y = size.width / 4;
    [self.confettiArea burstAt:CGPointMake(x, y) confettiWidth:5.0 numberOfConfetti:10];
}

- (void)conffettize3:(NSTimer*)timer{
    CGSize size = self.view.bounds.size;
    int x = size.width / 2;
    int y = (size.height / 4) * 3;
    [self.confettiArea burstAt:CGPointMake(x, y) confettiWidth:5.0 numberOfConfetti:10];
}

#pragma mark - Game control methods

- (void)dealInitialCards {
    self.gameState = InitialDealing;
    if (self.gameType == TypeMultiplayer) {
        [self sendGameStateMP:InitialDealing];
    }
    [self restartTimerWithInterval:1.0];
}


- (void)restartTimerWithInterval:(NSTimeInterval)interval {
    if (self.gameType == TypeMultiplayer) {
        [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(executeNextMoveMP:) userInfo:nil repeats:NO];
    } else {
        [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(executeNextMove:) userInfo:nil repeats:NO];
    }
}

- (void)dealCardToCurrentPlayer {
    CardView *cardView = [self.deck firstObject];
    [self.deck removeObjectAtIndex:0];
    [self.currentPlayer setCardView:cardView];
    if(self.gameType == TypeMultiplayer){
        [self dealCardToPlayerMP:self.currentPlayer.gcPlayer.playerID card:cardView.card];
    }
}


#pragma mark - Methods for helping evaluate final state of each round

- (NSInteger)checkForKing {
    NSInteger playerWithKing = NSNotFound;
    for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
        
    }
    
    return playerWithKing;
}

- (int)evaluateHappyKing {
    __block BOOL kingFound = NO;
    __block BOOL twoOfAKind = NO;
    __block BOOL threeOfAKind = NO;
    __block BOOL fourOfAKind = NO;
    int result = 0;
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    NSMutableDictionary *repeatedCardsCountDictionary = [NSMutableDictionary dictionary];
    for (int i = 0; i < _numberOfPlayer; i++) {
        Player *player = self.players[i];
        if (![indexSet containsIndex:player.cardView.card.rank]) {
            [indexSet addIndex:player.cardView.card.rank];
        } else {
            NSNumber *number = @(player.cardView.card.rank);
            NSNumber *repeatedCardCount = repeatedCardsCountDictionary[number];
            if (repeatedCardCount) {
                repeatedCardCount = @([repeatedCardCount intValue] + 1);
                repeatedCardsCountDictionary[number] = repeatedCardCount;
            } else {
                repeatedCardCount = @(2);
                repeatedCardsCountDictionary[number] = repeatedCardCount;
            }
        }
        if (player.cardView.card.rank == King)
        {
            kingFound = true;
        }
    }
    [indexSet enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSNumber *index = @(idx);
        NSNumber *repeatedCardCount = repeatedCardsCountDictionary[index];
        switch ([repeatedCardCount intValue]) {
            case 2:
                twoOfAKind = YES;
                break;
            case 3:
                threeOfAKind = YES;
                break;
            case 4:
                fourOfAKind = YES;
                break;
        }
    }];
    
    if (kingFound) {
        result |= ResultKing;
    }
    if (twoOfAKind) {
        result |= ResultPair;
    }
    if (threeOfAKind) {
        result |= ResultThreeOfAKind;
    }
    if (fourOfAKind) {
        result |= ResultFourOfAKind;
    }
    if (result == 0)
    {
        result = ResultNormal;
    }
    return result;
}

- (NSDictionary *) getNumberOfOcurrencesForEachRank {
    NSMutableDictionary *repeatedCardsCountDictionary = [NSMutableDictionary dictionary];
    
    for (int rank = Ace; rank <= King; rank++) {
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        for (int i = 0; i < _numberOfPlayer; i++) {
            Player *player = self.players[i];
            if (player.lives == 0)
                continue;
            if (player.cardView.card.rank == rank) {
                [indexSet addIndex:i];
            }
        }
        if (rank == King || [indexSet count] >= 2) {
            NSNumber *numberRank = @(rank);
            repeatedCardsCountDictionary[numberRank] = indexSet;
        }
    }
    return repeatedCardsCountDictionary;
}

- (Player*)getLowestRankCard:(NSArray *)repeatedRanks {
    Player *lowestRankPlayer = nil;
    for (Player *player in self.players) {
        if (player.lives == 0) continue;
        Rank currentRank = player.cardView.card.rank;
        if ([repeatedRanks indexOfObject:@(currentRank)] != NSNotFound) continue;
        if (!lowestRankPlayer) {
            lowestRankPlayer = player;
        } else if (player.cardView.card.rank < lowestRankPlayer.cardView.card.rank) {
            lowestRankPlayer = player;
        }
    }
    return lowestRankPlayer;
}

- (NSIndexSet*)indexesWithRank:(Rank)rank {
    NSMutableIndexSet *resultSet = [NSMutableIndexSet indexSet];
    for (int i; i < _numberOfPlayer ; i++) {
        Player *player = self.players[i];
        if (player.cardView.card.rank == rank) {
            [resultSet addIndex:i];
        }
    }
    return resultSet;
}

- (BOOL)readyGameForNextRound {
    if ([self getPlayersAvailable:false] == 0) {
        [self.nextRoundButton setEnabled:NO];
        return NO;
    }
    self.areAllCardFlipped = NO;
    self.dealer = self.dealer.nextPlayer;
    self.currentPlayer = self.dealer.nextPlayer;
    if (self.currentPlayer.lives == 0) {
        [self moveToNextPlayer];
    }
    int playersAvailable = [self getPlayersAvailable:true];
    
    [self positionDeck];
    [self.nextRoundButton setEnabled:NO];
    if (playersAvailable == 1) {
        Player *winnerPlayer = nil;
        for (Player *player in self.players) {
            if (player.lives != 0) {
                winnerPlayer = player;
                winnerPlayer.finalState = StateWinner;
                return NO;
            }
        }
    } else {
        if ([self.deck count] < playersAvailable) {
            int initial = self.deck.count % 5;
            NSMutableArray *newDeck = [self createDeck];
            [self.deck addObjectsFromArray:newDeck];
            [self addCardsToScreen:self.deck initialPositionValue:initial];
            return YES;
        }
    }
    return YES;
}

- (void)moveToNextPlayer {
    do {
        self.currentPlayer = self.currentPlayer.nextPlayer;
        if (self.currentPlayer.lives)
            break;
    }while(1);
}

#pragma mark - Setters

- (void)setCurrentPlayer:(Player *)currentPlayer {
    _currentPlayer = currentPlayer;
    if (self.gameState == MachineDeciding || self.gameState == UserDeciding) {
        for (int i = 0; i < _numberOfPlayer; i++) {
            Player *player = self.players[i];
            player.isMyTurn = player == _currentPlayer;
        }
    }
}

- (void)setDealer:(Player *)dealer {
    _dealer.isDealer = NO;
    _dealer = dealer;
    _dealer.isDealer = YES;
}

#pragma mark - Actions

- (IBAction)nextRountButtonPressed:(id)sender {
    if ([self readyGameForNextRound]) {
        self.gameState = InitialDealing;
        [self restartTimerWithInterval:1.0];
    } else { //Not enough cards to play
        
    }
}

- (IBAction)keepCard:(id)sender {
    if (self.gameState == UserDeciding) {
        [self.currentPlayer stopTimerForTurn];
        if (self.currentPlayer.isDealer) {
            self.gameState = FinishedRound;
        } else {
            self.gameState = MachineDeciding;
            [self moveToNextPlayer];
        }
        [self restartTimerWithInterval:1.0];
    }
}

- (IBAction)switchCardWithNextPlayer:(id)sender {
    if (self.gameState != UserDeciding)
        return;
    
    [self.currentPlayer stopTimerForTurn];
    [self sendSwitchCardAction:self.currentPlayer.gcPlayer.playerID];
    
    if (_currentPlayer.isDealer) {
        [self dealerKillMove:YES];
        self.gameState = FinishedRound;
        [self restartTimerWithInterval:1.0];
    } else {
        if (_currentPlayer.nextPlayer.canCardBeSwapped){ //Next player has a Queen
            BOOL picture = [_currentPlayer switchCardWithPlayer:_currentPlayer.nextPlayer];
            [self showPicture:picture];
            [self moveToNextPlayer];
            self.gameState = MachineDeciding;
            [self restartTimerWithInterval:1.0];
        } else {
            [_currentPlayer.nextPlayer showQueen];
            [self showMessageWithType:MessageQueen withObject:nil andCompletion:^{
                [self moveToNextPlayer];
                self.gameState = MachineDeciding;
                [self restartTimerWithInterval:1.0];
            }];
        }
    }
}

- (IBAction)backButtonPressed:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Rey Contento" message:@"¿Realmente quieres salir? Se perderán todos los avances del juego." delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"SÍ", nil];
    alertView.tag = kAlertViewExitGameTag;
    [alertView show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kAlertViewAfterQuickGameTag) { //after quick game is finished.
        if (buttonIndex == 0)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else if(buttonIndex == 1)
        {
            if (self.gameType == TypeQuick) {
                [self stopConffetti];
                _areAllCardFlipped = NO;
                [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
                self.gameState = Initializing;
                [self showBetScreen];
            } else {
                [self.players makeObjectsPerformSelector:@selector(resetToInitialState)];
                self.areAllCardFlipped = NO;
                self.dealer = self.dealer.nextPlayer;
                self.currentPlayer = self.dealer.nextPlayer;
                [self positionDeck];
                [self dealInitialCards];
            }
        }
    } else if (alertView.tag == kAlertViewExitGameTag) { //when touched "Go back"
        if (buttonIndex == 1) {
            if(_gameState != GameFinished){
                if(self.gameType == TypeQuick){
                    [[GCHelper sharedManager] incrementCoins:- self.wagerCoins];
                }else{
                    [[GCHelper sharedManager] incrementCoins:- [self getIndividualEntryWager:self.numberOfPlayer]];
                }
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } else if (alertView.tag == kAlertViewPlayerLostKeepTag) {
        if (buttonIndex == 0) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self nextRountButtonPressed:nil];
        }
    }
}

- (void)setGameState:(GameState)gameState {
    _gameState = gameState;
    if (self.userPlayer.isDealer) {
        [_switchCardButton setTitle:@"Matar" forState:UIControlStateNormal];
    } else {
        [_switchCardButton setTitle:@"Cambiar" forState:UIControlStateNormal];
    }
    
    [_switchCardButton setEnabled:NO];
    [_keepCardButton setEnabled:NO];
    [_nextRoundButton setEnabled:NO];

    if (_gameState == MachineDeciding) {
        
    } else if (_gameState == UserDeciding) {
        [_switchCardButton setEnabled:YES];
        [_keepCardButton setEnabled:YES];
    } else if (_gameState == FinishedRound) {
        if (self.gameType == TypeMultiplayer)
            [_nextRoundButton setEnabled: YES];
    } else if (_gameState == BetweenRounds) {
        secondsToNextRound = 3;
        [self restartTimerWithInterval:1.0f];
    }
}

- (NSString*)getStringMessageWithType:(MessageType)type andObject:(NSString*)object {
    NSString *message = nil;
    
    switch (type) {
        case MessageDamnedThrice:
            message = MESSAGE_DAMNED_THRICE;
            break;
        case MessageKing:
            message = MESSAGE_HAPPY_KING;
            break;
        case MessagePair:
            message = MESSAGE_FRIENDLY_PAIR;
            break;
        case MessagePlayerOut:
            message = MESSAGE_PLAYER_OUT;
            break;
        case MessageQueen:
            message = MESSAGE_QUEEN;
            break;
        case MessageFourOfAKind:
            message = MESSAGE_FOUR_OF_A_KIND;
            break;
        case MessageWinner:
            message = MESSAGE_WINNER;
            break;
        case MessagePicture:
            message = MESSAGE_PICTURE;
            break;
        case MessageMinusPlusOne:
            message = MESSAGE_MINUS_PLUS_ONE;
            break;
    }
    
    if (object) {
        message = [NSString stringWithFormat:message, object];
    }
    return message;
}

- (void)showMessage:(NSDictionary *)messageDict withCompletion:(ShowMessageCompletionhandler)completionHandler{
    MessageType type = [messageDict[@"type"] intValue];
    NSString *object = messageDict[@"object"];
    UIColor *color = [self colorWithMessageType:type];
    NSString *message = [self getStringMessageWithType:type andObject:object];
    [self showMessage:message andColor:color withCompletion:completionHandler];
}

- (void)showMessage:(NSString *)message  andColor:(UIColor*)color withCompletion:(ShowMessageCompletionhandler)completionHandler{
    __block UILabel *label = [[UILabel alloc] initWithFrame:(CGRect){{0,0},{400,200}}];
    label.center = self.view.center;
    [label setFont:_gameFont];
    [label setTextColor:color];
    [label setText:message];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:0];
    [self.view addSubview:label];
    label.transform = CGAffineTransformMakeScale(1.5, 1.5);
    [UIView animateWithDuration:1.0 animations:^{
        label.transform = CGAffineTransformMakeScale(0.8, 0.8);
        label.alpha = 0.3;
    } completion:^(BOOL finished) {
        [label removeFromSuperview];
        if (completionHandler) {
            completionHandler();
        }
    }];
    
}

- (void)showFinalMessages:(ShowMessageCompletionhandler)completionAfterShowing {
    if (self.finalRoundMessages.count == 0) {
        completionAfterShowing();
        return;
    }
    NSDictionary *messageDict = self.finalRoundMessages[0];
    [self showMessage:messageDict withCompletion:^{
        [self.finalRoundMessages removeObjectAtIndex:0];
        [self showFinalMessages:completionAfterShowing];
    }];
}

- (void)showMessageWithType:(MessageType)messageType withObject:(NSString*)object andCompletion:(ShowMessageCompletionhandler)completion{
    NSDictionary *messageDict = object ? @{@"type": @(messageType) , @"object": object} : @{@"type": @(messageType)};
    [self showMessage:messageDict withCompletion:completion];
}

- (void)enqueueFinalStateMessage:(MessageType)messageType withObject:(NSString*)object {
    NSDictionary *messageDict = nil;
    messageDict = object ? @{@"type": @(messageType) , @"object": object} : @{@"type": @(messageType)};;
    if ([self.finalRoundMessages indexOfObject:messageDict] == NSNotFound) {
        [self.finalRoundMessages addObject:messageDict];
    }
}

- (UIColor*)colorWithMessageType:(MessageType)type {
    switch (type) {
        case MessageDamnedThrice:
            return [UIColor colorWithRed:1.0 green:0.1 blue:0.1 alpha:1.0];
        case MessageKing:
            return [UIColor colorWithRed:0.33 green:0.55 blue:0.05 alpha:1.0];
        case MessagePair:
            return [UIColor colorWithRed:0.1 green:0.9 blue:0.4 alpha:1.0];
        case MessagePlayerOut:
            return [UIColor orangeColor];
        case MessageQueen:
            return [UIColor magentaColor];
        case MessageFourOfAKind:
            return [UIColor colorWithRed:0.1 green:0.2 blue:0.9 alpha:1.0];
        case MessageWinner:
            return [UIColor blueColor];
        case MessagePicture:
            return [UIColor whiteColor];
        case MessageMinusPlusOne:
            return [UIColor yellowColor];
    }
}

- (void)playerDidLost:(Player *)player {
    NSInteger index = [self.players indexOfObject:player];
    if (![self.koPlayers containsIndex:index]) {
        [self.koPlayers addIndex:index];
        [self enqueueFinalStateMessage:MessagePlayerOut withObject:player.playerName];
    }
}

- (void)playerTurnDidEnd:(Player *)player {
    if (player.isUser) {
        if (player.isDealer) {
            self.gameState = FinishedRound;
        } else {
            self.gameState = MachineDeciding;
            [self moveToNextPlayer];
        }
        [self restartTimerWithInterval:1.0];
    }
}

- (void)showPicture:(BOOL)picture {
    if (picture) {
        [self showMessageWithType:MessagePicture withObject:nil andCompletion:nil];
    }
}

- (int)getPlayersAvailable:(BOOL)resetPlayers {
    int playersAvailable = 0;
    for (Player *player in self.players) {
        if (resetPlayers) {
            [player reset];
        }
        if (player.lives != 0) {
            playersAvailable++;
        }
    }
    return playersAvailable;
}

- (Player *)getWinner {
    for (Player *player in self.players) {
        if (player.finalState == StateWinner) {
            return player;
        }
    }
    return nil;
}

- (BOOL)evaluateForPlusMinusOne {
    if (self.gameType == TypeQuick) return NO;
    Player *firstPlayer;
    Player *secondPlayer;
    for (Player *player in self.players) {
        if (player.lives != 0) {
            if (firstPlayer == nil) {
                firstPlayer = player;
            } else {
                secondPlayer = player;
                break;
            }
        }
    }
    return (firstPlayer.lives == 1 && secondPlayer.lives == 1) && abs((int)firstPlayer.cardView.card.rank - (int)secondPlayer.cardView.card.rank) == 1;
}


- (void)showBetScreen {
    BetView *betView = [BetView betView];
    betView.delegate = self;
    [betView showInView:self.view];
}

- (void)betView:(BetView*)view didMakeBet:(NSNumber*)coins {
    [self setupPlayersPositions];
    [self addCardsToScreen:self.deck initialPositionValue:0];
    self.wagerCoins = [coins integerValue];
    
    if (self.players) {
        [self.players makeObjectsPerformSelector:@selector(resetToInitialState)];
        self.areAllCardFlipped = NO;
        self.dealer = self.dealer.nextPlayer;
        self.currentPlayer = self.dealer.nextPlayer;
        [self positionDeck];
    } else {
        [self createPlayers];
        [self positionDeck];
    }
    [self dealInitialCards];
}

- (void)setWagerCoins:(NSUInteger)wagerCoins {
    _wagerCoins = wagerCoins;
    if (_wagerCoins >= 500) {
        [achievementsManager userBetMoreThan500:^(GKAchievement *achievement, NSError *error) {
            
        }];
    }
    NSUInteger currentCoins = [[GCHelper sharedManager] coins];
    self.lblCurrentCoins.text = @"Monedas:";
    self.lblCurrentCoinsValue.text = @(currentCoins).stringValue;
    
    if(wagerCoins > 0){
        if(self.gameType == TypeQuick){
            self.lblBetCoins.text = @"Apuesta:";
        }else{
            self.lblBetCoins.text = @"Premio:";
        }
        self.lblBetCoinsValue.text = @(wagerCoins).stringValue;
    }else{
        self.lblBetCoinsValue.hidden = TRUE;
        self.lblBetCoins.hidden = TRUE;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    NSNumber *newchange = [change objectForKey:NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"coins"]) {
        //if (self.gameType == TypeQuick) {
            
            [UIView animateWithDuration:0.15 animations:^{
                self.lblCurrentCoinsValue.text = [newchange stringValue];
                self.lblCurrentCoinsValue.transform = CGAffineTransformMakeScale(2.0, 2.0);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.15 animations:^{
                    self.lblCurrentCoinsValue.transform = CGAffineTransformIdentity;
                } completion:^(BOOL finished) {
                    
                }];
            }];
        //}
    } else if ([keyPath isEqualToString:@"diamonds"]) {
        
    }
}

- (void)dealerKillMove:(BOOL)isUser {
    CardView *tempCardView =  self.deck[0];
    [self.deck removeObjectAtIndex:0];
    
    
    _currentPlayer.killMove = YES;
    _currentPlayer.cardView = nil;
    _currentPlayer.cardView = tempCardView;
    _currentPlayer.changedCard = YES;
}

- (void)animateCoinsWinner {

}


- (NSInteger)getTotalWagerCoins:(NSInteger)numberOfPlayers withUsersWager:(BOOL)usersWager{
    NSInteger entryWager = [self getIndividualEntryWager:numberOfPlayers];
    return entryWager * (usersWager ? numberOfPlayers : (numberOfPlayers - 1));
}

- (NSInteger)getIndividualEntryWager:(NSInteger)numberOfPlayers {
    NSInteger entryWager = 0;
    NSInteger usersCoins = [[GCHelper sharedManager] coins];
    if (usersCoins != 0) {
        NSInteger tempEntry = 100;
        if (usersCoins >= 100)
            entryWager = tempEntry;
        else
            entryWager = usersCoins;
    } else {
        entryWager = 20;
    }
    return entryWager;
}

- (void)createConffettiAndAdddToView {
    self.confettiArea = [[L360ConfettiArea alloc] initWithFrame:self.view.bounds];
    self.confettiArea.delegate = self;
    [self.confettiArea setUserInteractionEnabled:NO];
    [self.view addSubview:self.confettiArea];
}

- (void)stopConffetti {
    if([timer1 isValid]) {
        [timer1 invalidate];
        timer1 = nil;
    }
    if([timer2 isValid]) {
        [timer2 invalidate];
        timer2 = nil;
    }
    if([timer3 isValid]) {
        [timer3 invalidate];
        timer3 = nil;
    }
    [self.confettiArea removeFromSuperview];
    self.confettiArea = nil;
}

#pragma mark GCHelperDelegate

- (void)matchStarted {
    NSLog(@"Match Started");
}

- (void)matchAboutToStart:(GKMatch*)match {
    NSLog(@"Match About to start");
}

- (void)matchEnded {
    NSLog(@"Match Ended");
}

- (void)match:(GKMatch *)match didReceiveData:(NSData *)data
   fromPlayer:(NSString *)playerID {
    NSString *recievedString = [NSString stringWithUTF8String:[data bytes]];
    
    NSLog(@"Did receive Data, %@", recievedString);
    
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Multiplayer" message:[NSString stringWithFormat:@"Did receive Data, %@", recievedString] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];*/
    
    NSError *jsonError;
    NSDictionary *message = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingMutableContainers
                                                              error:&jsonError];
    [self dispatchMessageMP:message];
}


#pragma mark - Multiplayer methods

- (void)initializePlayersMP:(NSDictionary*)data{
    NSArray *serverPlayers = data[@"HKPlayers"];
    
    _playersMP = [[NSMutableArray<GKPlayer*> alloc] initWithCapacity:serverPlayers.count];
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    for (int i = 0; i < serverPlayers.count; i++) {
        NSDictionary *playerData = serverPlayers[i];
        
        if([localPlayer.playerID isEqualToString:playerData[@"HKgc"]]){
            [(NSMutableArray<GKPlayer*>*)_playersMP addObject:localPlayer];
            _offsetMP = [(NSString*)playerData[@"HKTurn"] intValue];
            break;
        }
    }
    
    int pivotUp = _offsetMP + 1;
    
    while (pivotUp < serverPlayers.count) {
        NSDictionary *playerData = serverPlayers[pivotUp];
        for (int k = 0; k < self.match.players.count; k++) {
            
            if([self.match.players[k].playerID isEqualToString:playerData[@"HKgc"]]){
                [(NSMutableArray<GKPlayer*>*)_playersMP addObject:self.match.players[k]];
                break;
            }
        }
        pivotUp++;
    }
    
    int pivotDown = 0;
    
    while (pivotDown < _offsetMP) {
        NSDictionary *playerData = serverPlayers[pivotDown];
        
        for (int k = 0; k < self.match.players.count; k++) {
            
            if([self.match.players[k].playerID isEqualToString:playerData[@"HKgc"]]){
                [(NSMutableArray<GKPlayer*>*)_playersMP addObject:self.match.players[k]];
                break;
            }
        }
        pivotDown++;
    }
}

- (void) dispatchMessageMP:(NSDictionary*)message{
    NSLog(@"Message, %@", message);
    int choice = [message[kMessageTypeKey] intValue];
    switch (choice) {
        case MatchServer:
            NSLog(@"MatchServer Message");
            self.serverIDMP = message[@"HKgc"];
            break;
        case PlayerNumber:
            NSLog(@"PlayerNumber Message");
            [self initializePlayersMP:message];
            [self createPlayers];
            break;
            
        case Dealer:
            NSLog(@"Dealer Message");
            for (int i = 0; i < self.players.count; i++){
                if ([message[@"HKgc"] isEqualToString:((Player*)self.players[i]).gcPlayer.playerID]){
                    self.dealer = self.players[i];
                    self.currentPlayer = self.dealer.nextPlayer;
                    break;
                }
            }
            [self positionDeck];
            
            if([self.dealer.gcPlayer.playerID isEqualToString:self.userPlayer.gcPlayer.playerID] && self.gameState == Initializing){
                [self dealInitialCards];
            }
            break;
            
        case GameStateChanged:
            NSLog(@"Game State Message");
            [self setGameState:(GameState)[(NSString*)message[@"HKstate"] intValue]];
            break;
            
        case DealCard:
            NSLog(@"Deal card Message");
            
            for (int i = 0; i < self.players.count; i++){
                if ([message[@"HKto"] isEqualToString:((Player*)self.players[i]).gcPlayer.playerID]){
//                    Suit suit = [message[@"HKsuit"] intValue];
//                    Rank rank = [message[@"HKrank"] intValue];
//                    Card *card = [Card createCardWithSuit:suit andRank:rank];
//                    CardView *cardView = [CardView cardViewWithCard:card];
//                    
//                    [self.players[i] setCardView:cardView];
                    CardView *cardView = [self.deck firstObject];
                    [self.deck removeObjectAtIndex:0];
                    [self.players[i] setCardView:cardView];
                    break;
                }
            }
            
            break;
            
        case ResolutionRequest:
            NSLog(@"Resolution Message");
            if ([message[@"HKto"] isEqualToString:self.userPlayer.gcPlayer.playerID]){
                self.gameState = UserDeciding;
                [self.userPlayer startTimerForTurn];
            }
            break;
            
        case SwitchCard:
        {
            NSLog(@"Switch Message");
            Player *playerThatChanges = nil;
            
            for (int i = 0; i < self.players.count; i++){
                if ([message[@"HKto"] isEqualToString:((Player*)self.players[i]).gcPlayer.playerID]){
                    playerThatChanges = self.players[i];
                    break;
                }
            }
            
            if (playerThatChanges.isDealer) {
                [self dealerKillMove:YES];
                self.gameState = FinishedRound;
                [self restartTimerWithInterval:1.0];
            } else {
                if (playerThatChanges.nextPlayer.canCardBeSwapped){ //Next player has a Queen
                    BOOL picture = [playerThatChanges switchCardWithPlayer:playerThatChanges.nextPlayer];
                    [self showPicture:picture];
                    [self moveToNextPlayer];
                    self.gameState = MachineDeciding;
                    [self restartTimerWithInterval:1.0];
                } else {
                    [playerThatChanges.nextPlayer showQueen];
                    [self showMessageWithType:MessageQueen withObject:nil andCompletion:^{
                        [self moveToNextPlayer];
                        self.gameState = MachineDeciding;
                        [self restartTimerWithInterval:1.0];
                    }];
                }
            }
            break;
        }
        case SendDeck:
        {
            self.deck = [self deckFromMMessage:message];
            [self addCardsToScreen:self.deck initialPositionValue:0];
            [self positionDeck];
        }
            break;
        default:
            NSLog(@"Default'ed :(");
            break;
    }
}

-(void)numberPlayersMP{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[NSString stringWithFormat:@"%d",(int)PlayerNumber] forKey:kMessageTypeKey];
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < self.playersMP.count; i++) {
        NSMutableDictionary *playerEntry = [[NSMutableDictionary alloc] initWithCapacity:_playersMP.count];
        
        [playerEntry setValue:_playersMP[i].playerID forKey:@"HKgc"];
        [playerEntry setValue:[NSString stringWithFormat:@"%d",i] forKey:@"HKTurn"];
        [playerEntry setValue:_playersMP[i].alias forKey:@"HKalias"];
        
        [list addObject:playerEntry];
    }
    
    NSError *error;
    
    [params setValue:list forKey:@"HKPlayers"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            // handle the error
            NSLog(@"Error");
        } else {
            NSLog(@"Booya!");
        }
    }
    
}

-(BOOL)localPlayerWillBeServer{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)MatchServer] forKey:kMessageTypeKey];
    [params setValue:localPlayer.playerID forKey:@"HKgc"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        
        //NSData *data = [[NSString stringWithFormat:@"{messageType:'server', gc:'%@'}", localPlayer.playerID] dataUsingEncoding:NSUTF8StringEncoding];
        //NSArray<GKPlayer*> *playerEnv = @[self.match.players[0]];
        
        //[self.match sendData:data toPlayers:playerEnv dataMode:GKMatchSendDataReliable error:&error];
        
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            // handle the error
            NSLog(@"Error");
        } else {
            NSLog(@"Booya!");
            return TRUE;
        }
    }
    
    return FALSE;
}

- (void)announceDealerMP{
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)Dealer] forKey:kMessageTypeKey];
    [params setValue:self.dealer.gcPlayer.playerID forKey:@"HKgc"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Dealer announced");
        }
    }
}

- (void)sendGameStateMP:(GameState)state{
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)GameStateChanged] forKey:kMessageTypeKey];
    [params setValue:[NSString stringWithFormat:@"%d",(int)state] forKey:@"HKstate"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Game state changed");
        }
    }
}

- (void)executeNextMoveMP:(NSTimer*)timer {
    BOOL restartTimer = NO;
    CGFloat nextTimer = 1.0f;
    BOOL picture = NO;
    switch(_gameState) {
        case InitialDealing:
        {
            [self dealCardToCurrentPlayer];
            //BOOL wasLastPlayerDealer = self.currentPlayer.isDealer;
            //if (wasLastPlayerDealer) {
            if (self.currentPlayer.nextPlayer.cardView != Nil) {
                /*if (self.currentPlayer.nextPlayer.isUser) {
                 self.gameState = UserDeciding;
                 } else {
                 self.gameState = RemoteUserDeciding;
                 [self askForResolution:self.currentPlayer.nextPlayer.gcPlayer.playerID];
                 //restartTimer = YES;
                 }
                 if (HAS_RESULT_VALUE([self evaluateHappyKing], ResultKing)) {
                 self.gameState = FinishedRound;
                 restartTimer = YES;
                 }*/
                self.gameState = RemoteUserDeciding;
                restartTimer = YES;
            } else {
                restartTimer = YES;
                nextTimer = CARD_DEAL_TIMER;
            }
            [self moveToNextPlayer];
        }
            break;
        case RemoteUserDeciding:
        {
            [self askForResolution:self.currentPlayer.gcPlayer.playerID];
            /*if (!_currentPlayer.isDealer) {
             
             BOOL canCardFromNextPlayerBeSwapped = [_currentPlayer.nextPlayer canCardBeSwapped];
             if (_currentPlayer.shouldChange  && canCardFromNextPlayerBeSwapped ) {
             Player *nextPlayer = _currentPlayer.nextPlayer;
             picture = [_currentPlayer switchCardWithPlayer:nextPlayer];
             [self showPicture:picture];
             _currentPlayer.changedCard = YES;
             } else if (_currentPlayer.cardView.card.rank <= 3 && canCardFromNextPlayerBeSwapped) {
             Player *nextPlayer = _currentPlayer.nextPlayer;
             picture = [_currentPlayer switchCardWithPlayer:nextPlayer];
             [self showPicture:picture];
             _currentPlayer.changedCard = YES;
             } else if (_currentPlayer.cardView.card.rank <=  7) { //if it's the first dealing and the card is below 6 we make a random decision to switch cards.
             BOOL changeCard = arc4random_uniform(5) < 2;
             if (changeCard && canCardFromNextPlayerBeSwapped) {
             picture = [_currentPlayer switchCardWithPlayer:_currentPlayer.nextPlayer];
             [self showPicture:picture];
             }
             _currentPlayer.changedCard = changeCard;
             } else if (_currentPlayer.cardView.card.rank == Queen) {
             Player *previousPlayer = _currentPlayer.previousPlayer;
             BOOL changeCard = arc4random_uniform(5) < 2;
             if (previousPlayer.changedCard && changeCard) {
             picture = [previousPlayer switchCardWithPlayer:previousPlayer.previousPlayer];
             [self showPicture:picture];
             previousPlayer.changedCard = NO;
             }
             }
             [self moveToNextPlayer];
             restartTimer = YES;
             if (self.currentPlayer.isUser) {
             self.gameState = UserDeciding;
             restartTimer = NO;
             }
             
             } else {
             if (_currentPlayer.shouldChange) {
             [self dealerKillMove:NO];
             } else if (_currentPlayer.cardView.card.rank <= 3) {
             [self dealerKillMove:NO];
             } else if (_currentPlayer.cardView.card.rank <= 7) { //if it's the first dealing and the card is below 6 we make a random decision to switch cards.
             BOOL changeCard = arc4random_uniform(5) < 2;
             if (changeCard) {
             [self dealerKillMove:NO];
             }
             _currentPlayer.changedCard = changeCard;
             } else if (_currentPlayer.cardView.card.rank == Queen) {
             Player *previousPlayer = _currentPlayer.previousPlayer;
             BOOL changeCard = arc4random_uniform(5) < 2;
             if (previousPlayer.changedCard && changeCard) {
             picture = [previousPlayer switchCardWithPlayer:previousPlayer.previousPlayer];
             [self showPicture:picture];
             previousPlayer.changedCard = NO;
             }
             }
             _currentPlayer.isMyTurn = NO;
             self.gameState = FinishedRound;
             restartTimer = YES;
             }*/
        }
            break;
        case FinishedRound: {
            if (!self.areAllCardFlipped) {
                [self.players makeObjectsPerformSelector:@selector(revealCard)];
                self.areAllCardFlipped = YES;
                restartTimer = YES;
                nextTimer = 1.0;
            } else {
                [achievementsManager updateTotalRoundsPlayed:achievementsManager.totalRoundsPlayed + 1 withHandler:^(GKAchievement *achievement, NSError *error) {
                    NSLog(@"Error: %@", [error localizedDescription]);
                }];
                
                NSMutableDictionary *occurrencesByRank = [[self getNumberOfOcurrencesForEachRank] mutableCopy];
                if ([occurrencesByRank[@(King)] count] != 0) { //there is at least one King
                    [occurrencesByRank[@(King)] enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                        Player *player = self.players[idx];
                        player.finalState = StateHasKing;
                    }];
                    [self enqueueFinalStateMessage:MessageKing withObject:nil];
                }
                if ([occurrencesByRank[@(King)] count] == 1)
                    [occurrencesByRank removeObjectForKey:@(King)];
                
                
                NSArray *keys = [occurrencesByRank allKeys];
                __block BOOL allSaved = NO;
                for (id key in keys) {
                    NSIndexSet *set = occurrencesByRank[key];
                    __block NSInteger repetitions = [set count];
                    [set enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                        FinalState finalState = (FinalState)repetitions;
                        if (finalState == StateSamePair) {
                            [self enqueueFinalStateMessage:MessagePair withObject:nil];
                        } else if (finalState == StateSameThreeOfAKind) {
                            [self enqueueFinalStateMessage:MessageDamnedThrice withObject:nil];
                        } else if (finalState == StateSameFourOfAKind) {
                            [self enqueueFinalStateMessage:MessageFourOfAKind withObject:nil];
                        }
                        Player *player = self.players[idx];
                        player.finalState = (FinalState)repetitions;
                        allSaved = player.finalState == StateSameThreeOfAKind; //Tercia maldita se salvan todos los demás.
                        
                        Player *userPlayer = self.userPlayer;
                        if (finalState == StateSameThreeOfAKind) {
                            if (userPlayer.finalState == StateSameThreeOfAKind) {
                                [achievementsManager userWonThreeOfAKind:^(GKAchievement *achievement, NSError *error) {
                                    
                                }];
                            }
                        } else if (finalState == StateSamePair) {
                            if (userPlayer.finalState == StateSamePair) {
                                [achievementsManager friendlyPair:^(GKAchievement *achievement, NSError *error) {
                                    
                                }];
                            }
                        }
                    }];
                }
                
                Player *player = [self getLowestRankCard: [occurrencesByRank allKeys]];
                if (player.finalState == StateUndefined && !allSaved) {
                    if ([self getPlayersAvailable:false] == 2
                        && [self evaluateForPlusMinusOne]) {
                        [self.players makeObjectsPerformSelector:@selector(incrementOneLife)];
                        [self.koPlayers removeAllIndexes];
                        [self enqueueFinalStateMessage:MessageMinusPlusOne withObject:nil];
                    } else {
                        player.finalState = StateLowest;
                        if(player == self.userPlayer && player.cardView.card.rank == King) {
                            [achievementsManager userLostWithKing:^(GKAchievement *achievement, NSError *error) {
                                
                            }];
                        }
                    }
                }
                if (self.userPlayer.finalState != StateLowest && self.userPlayer.finalState != StateOut && self.userPlayer.finalState != StateUndefined) {
                    if (self.gameType != TypeQuick) {
                        /*NSUInteger coins = [GCHelper sharedManager].coins;
                         coins += self.userPlayer.cardView.card.rank;
                         [[GCHelper sharedManager] setCoins:coins];
                         [achievementsManager updateTotalCoinsWon:achievementsManager.totalCoinsWon + coins withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                         
                         }];*/
                        if ([[GCHelper sharedManager] gameCenterEnabled]) {
                            NSUInteger score = [GCHelper sharedManager].mainLeaderboardScore;
                            score += self.userPlayer.cardView.card.rank;
                            [[GCHelper sharedManager] updateScoreAndSend:score];
                        }
                    }
                    
                    
                    if (occurrencesByRank[@(Queen)] != 0) {
                        [achievementsManager userKilledQueen:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    }
                    if (self.userPlayer.cardView.card.rank == Seven) {
                        [achievementsManager userWonWith7:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    } else if (self.userPlayer.cardView.card.rank == Two) {
                        [achievementsManager userWonWith2:^(GKAchievement *achievement, NSError *error) {
                            
                        }];
                    }
                }
                [self showFinalMessages:^{
                    if ([self getPlayersAvailable:NO] == 1) {
                        Player *winnerPlayer = nil;
                        for (Player *player in self.players) {
                            if (player.lives != 0) {
                                winnerPlayer = player;
                                winnerPlayer.finalState = StateWinner;
                            }
                        }
                        self.gameState = GameFinished;
                        [self restartTimerWithInterval:nextTimer];
                    } else {
                        //                        if (self.gameType == TypeMultiplayer)
                        [self.nextRoundButton setEnabled: YES];
                        //                        else
                        //                            self.gameState = BetweenRounds;
                        
                    }
                }];
                
            }
            //            int result = [self evaluateHappyKing];
            //            if (HAS_RESULT_VALUE(result, ResultKing)) {
            //                NSIndexSet *indexesForKings = [self indexesWithRank:King];
            //            }
            
            //            Player *winnerPlayer = self.players[winnerPlayerIndex];
            //            winnerPlayer.finalState = StateHasKing;
        }
            break;
        case GameFinished:
        {
            Player *winner = [self getWinner];
            
            if (self.gameType == TypeQuick) {
                if (winner == self.userPlayer) {
                    
                    [[GCHelper sharedManager] incrementCoins: self.wagerCoins];
                    
                    [achievementsManager updateTotalCoinsWon:(achievementsManager.totalCoinsWon + self.wagerCoins) withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                        
                    }];
                }  else {
                    [[GCHelper sharedManager] incrementCoins:- self.wagerCoins];
                }
            } else {
                if (winner == self.userPlayer) {
                    long coinsWon = [self getTotalWagerCoins:self.numberOfPlayer withUsersWager:false];
                    [[GCHelper sharedManager] incrementCoins: coinsWon];
                    
                    [achievementsManager updateTotalCoinsWon:(achievementsManager.totalCoinsWon + coinsWon) withComletionHandler:^(GKAchievement *achievement, NSError *error) {
                        
                    }];
                }  else {
                    [[GCHelper sharedManager] incrementCoins:- [self getIndividualEntryWager:self.numberOfPlayer]];
                }
            }
            [self showMessageWithType:MessageWinner withObject:winner.playerName andCompletion:^{
                if (winner == self.userPlayer) {
                    [self createConffettiAndAdddToView];
                    
                    timer1 = [NSTimer scheduledTimerWithTimeInterval: 1.5
                                                              target: self
                                                            selector: @selector(conffettize:)
                                                            userInfo: nil
                                                             repeats: YES];
                    
                    //                    timer2 = [NSTimer scheduledTimerWithTimeInterval: 1.5
                    //                                                              target: self
                    //                                                            selector: @selector(conffettize2:)
                    //                                                            userInfo: nil
                    //                                                             repeats: YES];
                    //
                    //                    timer3 = [NSTimer scheduledTimerWithTimeInterval: 1.5
                    //                                                              target: self
                    //                                                            selector: @selector(conffettize3:)
                    //                                                            userInfo: nil
                    //                                                             repeats: YES];
                }
            }];
        }
            break;
        default:
            break;
            
    }
    
    if (restartTimer) {
        [self restartTimerWithInterval:nextTimer];
    } else {
        if (self.gameState == UserDeciding) {
            [self.userPlayer startTimerForTurn];
        }
    }
}

- (void)askForResolution:(NSString*)playerID {
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)ResolutionRequest] forKey:kMessageTypeKey];
    [params setValue:playerID forKey:@"HKto"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Game state changed");
        }
    }
}

- (void)dealCardToPlayerMP:(NSString*)playerID card:(Card*)card{
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)DealCard] forKey:kMessageTypeKey];
    [params setValue:playerID forKey:@"HKto"];
    [params setValue:[NSString stringWithFormat:@"%d",(int)card.suit] forKey:@"HKSuit"];
    [params setValue:[NSString stringWithFormat:@"%d",(int)card.rank] forKey:@"HKRank"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Card Sent");
        }
    }
    
}

- (void)sendSwitchCardAction:(NSString*)playerID {
    NSError *error;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setValue:[NSString stringWithFormat:@"%d",(int)SwitchCard] forKey:kMessageTypeKey];
    [params setValue:playerID forKey:@"HKto"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Card Sent");
        }
    }
    
}

- (void)sendDeckToOtherPlayers:(NSArray*)deck {
    NSMutableArray *deckToSend = [NSMutableArray array];
    for (CardView *card in deck ) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        dictionary[@"GKrank"] = @(card.card.rank);
        dictionary[@"GKsuit"] = @(card.card.suit);
        [deckToSend addObject:dictionary];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSString stringWithFormat:@"%d",(int)SendDeck] forKey:kMessageTypeKey];
    [params setValue:deckToSend forKey:@"HKdeck"];
    NSLog(@"Message, %@", params);
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                       options:0 error:&error];
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        [self.match sendDataToAllPlayers:jsonData withDataMode:GKMatchSendDataReliable error:&error];
        if (error != nil)
        {
            NSLog(@"Error");
        } else {
            NSLog(@"Game state changed");
        }
    }

}

- (NSMutableArray*)deckFromMMessage:(NSDictionary*)message {
    NSMutableArray *deck = [NSMutableArray array];
    NSArray *cardsArray = message[@"HKdeck"];
    for (NSDictionary *cardDict in cardsArray) {
        Rank rank = [cardDict[@"GKrank"] intValue];
        Suit suit = [cardDict[@"GKsuit"] intValue];
        Card *card = [Card createCardWithSuit:suit andRank:rank];
        CardView *cardView = [CardView cardViewWithCard:card];
        [deck addObject:cardView];
    }
    return deck;
}

@end
