//
//  MPRoomsViewController.m
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 07/01/16.
//  Copyright © 2016 Devhive. All rights reserved.
//

#import "MPRoomsViewController.h"
#import "MenuTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "GCHelper.h"
#import "InAppPurchasesHelper.h"
#import "PListData.h"
#import "Definitions.h"
#import "ThemesStoreViewController.h"
#import "UIView+Stuff.h"
#import "ScoreView.h"
#import "GCHelper.h"

@interface MPRoomsViewController()

@end

@implementation MPRoomsViewController
-(void)viewWillAppear:(BOOL)animated {
    
    NSString *ImageProfileTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageProfileTheme != nil)
    {
        [self.backgroundImageView setImage : [UIImage imageNamed:ImageProfileTheme]];
    }
    
    
    
    UINib *cellNib = [UINib nibWithNibName:@"RoomCellView" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"cvCell"];
    //Layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(200, 200)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@%@", BASE_URL, @"api/rooms", [NSString stringWithFormat:@"?access_token=%@", [[GCHelper sharedManager] token]]];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response ObjeCt: %@", responseObject);
        NSLog(@"Is NSArray: %d", [responseObject isKindOfClass:[NSArray class]]);
        _rooms = responseObject;
        NSLog(@"NSArray length: %lu", [_rooms count]);
        NSLog(@"NSArray 2dn: %@", _rooms[1]);
        NSLog(@"%@ (%@)", _rooms[1][@"name"], _rooms[1][@"bet"]);
        [_collectionView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];

}

-(void)viewDidLoad:(BOOL)animated {

}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return _rooms.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellData = [NSString stringWithFormat:@"%@\n\n(Entrada %@ monedas)", _rooms[indexPath.item][@"name"], _rooms[indexPath.item][@"bet"]];
    
    static NSString *cellIdentifier = @"cvCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIFont *font = kMainFontWithSize(20);
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag: 100];
    [titleLabel setText:cellData];
    titleLabel.font = font;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // If you need to use the touched cell, you can retrieve it like so
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Room" message:[NSString stringWithFormat:@"%@ (%@)", _rooms[indexPath.item][@"name"], _rooms[indexPath.item][@"bet"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
    NSDictionary *room = _rooms[indexPath.item];
    NSNumber *players = (NSNumber*)room[@"players"];
    NSUInteger roomId = (NSUInteger)[room[@"roomId"] intValue];
    _players = (NSInteger)[room[@"players"] intValue];
    
    if ([GCHelper sharedManager].gameCenterEnabled) {
        [[GCHelper sharedManager]
         findMatchWithMinPlayers:players.intValue maxPlayers:players.intValue viewController:self roomId:roomId];
    }
    
    NSLog(@"touched cell %@ at indexPath %@", cell, indexPath);
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ToMultiplayerMainGame"]) {
        ViewController *gameViewController = segue.destinationViewController;
        gameViewController.gameType = TypeMultiplayer;
        gameViewController.numberOfPlayer = _players;
        [GCHelper sharedManager].delegate = gameViewController;
    }
}

- (void)startMultiplayerMatch {
    [self performSegueWithIdentifier:@"ToMultiplayerMainGame" sender:self];
}

- (IBAction)quitViewPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
