//
//  Card.h
//  HappyKing
//
//  Created by Leonardo Cid on 29/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef enum {
    Spades = 0,
    Cloves,
    Hearts,
    Diamonds
}Suit;

typedef enum {
    Ace = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
}Rank;

@interface Card : NSObject
@property (nonatomic) Suit suit;
@property (nonatomic) Rank rank;

+(Card*)createCardWithSuit:(Suit)suit andRank:(Rank)rank;

- (id)initWithSuit:(Suit)suit andRank:(Rank)rank;

- (BOOL)isBlack;
- (NSString *)rankTitle;
- (UIImage *)suitImage;
- (UIColor*)suitColor;
@end
