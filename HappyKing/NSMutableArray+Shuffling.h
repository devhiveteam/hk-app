//
//  NSMutableArray+Shuffling.h
//  HappyKing
//
//  Created by Leonardo Cid on 29/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffling)

- (void)shuffle;

@end
