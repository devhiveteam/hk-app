//
//  InAppPurchasesHelper.m
//  HappyKing
//
//  Created by Leonardo Cid on 03/07/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "InAppPurchasesHelper.h"
#import "GCHelper.h"

#define SANDBOX_SHARED_SECRET @"4ecc091232ba467eaea8d7d6652cdb6a"
// Add to top of file

#define COINS1 @"com.happyking.HappyKing.coins1"
#define COINS2 @"com.happyking.HappyKing.coins2"
#define COINS3 @"com.happyking.HappyKing.coins3"
#define COINS4 @"com.happyking.HappyKing.coins4"
#define COINS5 @"com.happyking.HappyKing.coins5"
#define COINS6 @"com.happyking.HappyKing.coins6"

@implementation InAppPurchasesHelper {
    // 3
    SKProductsRequest * _productsRequest;
    // 4
    RequestProductsCompletionHandler _completionHandler;
    NSSet *_productIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet*)productIdentifiers {
    self = [super init];
    if (self) {
        _productIdentifiers = productIdentifiers;
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}

+ (InAppPurchasesHelper *)sharedInstance {
    static dispatch_once_t once;
    static InAppPurchasesHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      COINS1,
                                      COINS2,
                                      COINS3,
                                      COINS4,
                                      COINS5,
                                      COINS6,
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    
    NSLog(@"%@", response.invalidProductIdentifiers);
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}



// Add new method
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    int coins = [self getNumberOfCoinsForProductId:productIdentifier];
    [[GCHelper sharedManager] incrementCoins: coins];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@(coins), @"coins", productIdentifier, @"productIdentifier", nil]];
    
}

- (int)getNumberOfCoinsForProductId:(NSString*) productId {
    if ([productId isEqualToString:COINS1]) {
        return 2250;
    } else if ([productId isEqualToString:COINS2]){
        return 5750;
    } else if ([productId isEqualToString:COINS3]) {
        return 12750;
    } else if ([productId isEqualToString:COINS4]) {
        return 32250;
    } else if ([productId isEqualToString:COINS5]){
        return 77000;
    } else if ([productId isEqualToString:COINS6]) {
        return 157000;
    }
    
    return 0;
}

@end
