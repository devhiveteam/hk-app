//
//  TutorialViewController.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/14/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  View Controller que muestra una pequeña guía acerca de las reglas del juego.
 */
@interface TutorialViewController : UIViewController

@end
