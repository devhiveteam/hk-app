//
//  TutorialViewController.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/14/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@end

@implementation TutorialViewController

@end
