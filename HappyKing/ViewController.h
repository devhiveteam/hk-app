//
//  ViewController.h
//  HappyKing
//
//  Created by Leonardo Cid on 28/04/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "Player.h"
#import "Card.h"
#import "CardView.h"
#import "GCHelper.h"
#import "BetView.h"
#import "AchievementsManager.h"
#import "Definitions.h"
#import "L360ConfettiArea.h"


typedef enum {
    Initializing = 0,
    InitialDealing,
    UserDeciding,
    MachineDeciding,
    FinishedRound,
    GameFinished,
    RemoteUserDeciding,
    BetweenRounds
}GameState;

typedef enum {
    MatchServer,
    PlayerNumber,
    Dealer,
    GameStateChanged,
    DealCard,
    ResolutionRequest,
    SwitchCard,
    SendDeck
}MultiplayerMessageType;

typedef enum {
    None = 0,
    ResultNormal = 1<<1,
    ResultKing = 1<<2,
    ResultPair = 1<<3,
    ResultThreeOfAKind = 1<<4,
    ResultFourOfAKind = 1<<5,
}GameResult;

typedef enum {
    MessageKing = 0,
    MessagePair,
    MessageDamnedThrice,
    MessageFourOfAKind,
    MessagePlayerOut,
    MessageWinner,
    MessageQueen,
    MessagePicture,
    MessageMinusPlusOne,
}MessageType;


#define HAS_RESULT_VALUE(result, flag) ((result & flag) != 0)
#define IS_RESULT_ONLY_VALUE(result, flag) ((result & flag) == flag)
#define REPEATED_

#define CARD_DEAL_TIMER 0.55

@interface ViewController : UIViewController<PlayerDelegate, GCHelperDelegate, BetViewDelegate, L360ConfettiAreaDelegate> 
{
    AchievementsManager *achievementsManager;
    NSTimer *timer1, *timer2, *timer3;
    NSInteger secondsToNextRound;
    BOOL hasStayInGameMessageBeenShown;
}
@property (nonatomic) NSInteger numberOfPlayer;
@property (nonatomic) GameType gameType;
@property (nonatomic, strong) GKMatch *match;

@end

