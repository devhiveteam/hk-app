//
//  ScoresManager.m
//  HappyKing
//
//  Created by Leonardo Cid on 02/07/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import "GCHelper.h"
#import "MenuViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "AchievementsManager.h"
#define SAVE_COINS 0

@interface GCHelper ()
@property (nonatomic, strong) NSMutableDictionary *playersDict;
@end

@implementation GCHelper
@synthesize delegate;
@synthesize playersDict;
+ (GCHelper*)sharedManager {
    static GCHelper *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[GCHelper alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    if (self) {
        _mainLeaderboardScore = 0;
        _token = [[NSUserDefaults standardUserDefaults] objectForKey:@"oauthToken"];
        _refreshToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"oauthRefreshToken"];
        NSNumber *coinsNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"numberOfCoins"];
        if (coinsNumber) {
            _coins = [coinsNumber intValue];
        } else {
            _coins = 0;
        }
        /*_diamonds = [[NSUserDefaults standardUserDefaults] integerForKey:@"numberOfDiamonds"];*/
        /*self.secretPass = [self loadValueWithKey:kSecretKey];
        self.uuid = [self loadValueWithKey:kUUIDKey];
        if (!self.uuid) {
            [self requestId];
        }*/
    }
    return self;
}

- (void)sendScoreToLeaderboard:(NSUInteger)theScore toLeaderboard:(NSString*)leaderboardId {
    GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:leaderboardId];
    score.value = (int64_t) theScore;
    
    [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (void)setCoins:(NSUInteger)coins {
    _coins = coins;
    
    if (SAVE_COINS) {
        [[NSUserDefaults standardUserDefaults] setInteger:coins forKey:@"numberOfCoins"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *url = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PATH_GET_GOINS, [NSString stringWithFormat:@"?access_token=%@&coins=%lu", self.token, coins]];
        [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Response ObjeCt setCoins: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error setCoins: %@", [error localizedDescription]);
        }];
    }

}

- (void)incrementCoins:(NSInteger)coins {
    NSInteger integer = (NSInteger)self.coins + coins;
    if (integer < 0)
        integer = 0;
    self.coins = (NSUInteger)integer;
}

- (void)updateScoreAndSend:(NSUInteger)newScore {
    self.mainLeaderboardScore = newScore;
    [self sendScoreToLeaderboard:_mainLeaderboardScore toLeaderboard:_leaderboardIdentifier];
}

#pragma mark - GameCenter web service calls
-(void)authenticateLocalPlayer:(void(^)(UIViewController *viewController, NSError *error))completionHandler{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        completionHandler(viewController, error);
        if (error) {
            NSLog(@"error: %@", [error localizedDescription]);
        }
        if (viewController == nil){
            if ([GKLocalPlayer localPlayer].authenticated) {
                _gameCenterEnabled = YES;
                
                [AchievementsManager sharedManager];
                /*if (!self.secretPass) {
                    [self registerUser];
                } else if (!self.token ) {
                    [self getOauthToken];
                }*/
                if(self.token){
                    [self updateGCId:^{
                        
                    }];
                }
                
                NSLog(@"Alias: %@; Id: %@", [GKLocalPlayer localPlayer].alias, [[GKLocalPlayer localPlayer] playerID]);
                
                
                // Get the default leaderboard identifier.
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                    
                    if (error != nil) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    else{
                        [GCHelper sharedManager].leaderboardIdentifier = leaderboardIdentifier;
                        if ([[GCHelper sharedManager] mainLeaderboardScore] == 0) {
                            GKLeaderboard *leaderboardRequest = [[GKLeaderboard alloc] init];
                            if (leaderboardRequest != nil)
                            {
                                leaderboardRequest.identifier = [GCHelper sharedManager].leaderboardIdentifier;
                                
                                [leaderboardRequest loadScoresWithCompletionHandler: ^(NSArray *scores, NSError *error) {
                                    if (error != nil)
                                    {
                                        NSLog(@"%@", [error localizedDescription]);
                                    }
                                    if (scores != nil)
                                    {
                                        int64_t scoreInt = leaderboardRequest.localPlayerScore.value;
                                        [[GCHelper sharedManager] setMainLeaderboardScore:scoreInt];
                                    }
                                }];
                            }
                        }
                    }
                }];
            }
            else{
                _gameCenterEnabled = NO;
                /*if (!self.secretPass) {
                    [self registerUser];
                } else if (!self.token ) {
                    [self getOauthToken];
                }*/
            }
        }
    };
}

- (void)findMatchWithMinPlayers:(int)minPlayers
                     maxPlayers:(int)maxPlayers
                 viewController:(UIViewController *)viewController
                         roomId:(NSUInteger)roomId {
    if (!_gameCenterEnabled) return;
    
    presentingViewController = viewController;
    
    GKMatchRequest *request = [[GKMatchRequest alloc] init];
    int maxPlayer = [GKMatchRequest maxPlayersAllowedForMatchOfType:GKMatchTypePeerToPeer];
    request.minPlayers = minPlayers;
    request.maxPlayers = maxPlayers > maxPlayer ? maxPlayer : maxPlayers;
    request.defaultNumberOfPlayers = minPlayers;
    request.playerGroup = roomId;
    
    GKMatchmakerViewController *mmvc =
    [[GKMatchmakerViewController alloc]
     initWithMatchRequest:request];
    mmvc.matchmakerDelegate = self;
//    mmvc.turnBasedMatchmakerDelegate = self;
//    mmvc.showExistingMatches = YES;
    
    [presentingViewController presentViewController:mmvc animated:YES completion:nil];
}

#pragma mark - Oauth Web service calls

- (void)registerUser:(void (^)())success {
    NSString *playerId = [[GKLocalPlayer localPlayer] playerID];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, PATH_REGISTER];
    if (playerId){
        [manager POST:url parameters:@{@"apiid" : self.uuid, @"gcid" : playerId}  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Response Objest: %@", responseObject);
            NSDictionary *response = responseObject;
            if ([response [@"status"] isEqualToString:@"OK"]) {
                self.secretPass = response[@"secret"];
                //[self getOauthToken];
                success();
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    } else {
        [manager POST:url parameters:@{@"apiid" : self.uuid}  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Response Objest: %@", responseObject);
            NSDictionary *response = responseObject;
            if ([response [@"status"] isEqualToString:@"OK"]) {
                self.secretPass = response[@"secret"];
                //[self getOauthToken];
                success();
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
    }
}

- (void)getOauthToken:(void (^)())success {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, PATH_GET_TOKEN];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    [manager POST:url parameters:@{@"grant_type" : @"password", @"client_id" : OAUTH_CLIENT_ID, @"client_secret": OAUTH_CLIENT_SECRET, @"username" : self.uuid, @"password" : self.secretPass}  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Objest: %@", responseObject);
        NSDictionary *response = responseObject;
        if (response [@"access_token"]) {
            self.token = response[@"access_token"];
            self.refreshToken = response[@"refresh_token"];
            success();
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error debugDescription]);
    }];
    
//    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, PATH_GET_TOKEN];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
//    // Set post method
//    [request setHTTPMethod:@"POST"];
//    // Set header to accept JSON request
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    NSDictionary *parameters = @{@"grant_type" : @"password", @"clientId" : OAUTH_CLIENT_ID, @"client_secret": OAUTH_CLIENT_SECRET, @"username" : self.uuid, @"password" : self.secretPass};
//    NSError *error = nil;
//    NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
//    [request setHTTPBody:data];
//    
//
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    operation.responseSerializer = [AFJSONResponseSerializer serializer];
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
//        NSLog(@"Response Object: %@", responseObject);
//        NSDictionary *response = responseObject;
//        if (response [@"access_token"]) {
//            self.token = response[@"access_token"];
//            self.refreshToken = response[@"refresh_token"];
//            success();
//        }
//    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
//        NSLog(@"Error: %@", [error debugDescription]);
//    }];
//    [operation start];
}

- (void)updateGCId:(void (^)())success {
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PATH_GET_SAVE, [NSString stringWithFormat:@"?access_token=%@&gcid=%@", self.token, localPlayer.playerID]];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response ObjeCt: %@", responseObject);
        //NSDictionary *response = responseObject;
        //if (response [@"access_token"]) {
        //self.token = response[@"access_token"];
        success(responseObject);
        //}
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
}

- (void)refreshOauthToken:(void (^)())success {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, PATH_GET_TOKEN];
    [manager POST:url parameters:@{@"grant_type" : @"refresh_token", @"client_id" : OAUTH_CLIENT_ID, @"client_secret": OAUTH_CLIENT_SECRET, @"refresh_token" : self.refreshToken}  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Objest: %@", responseObject);
        NSDictionary *response = responseObject;
        if (response [@"access_token"]) {
            self.token = response[@"access_token"];
            self.refreshToken = response[@"refresh_token"];
            success();
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
}

- (void)getUserInfo: (void (^)(id responseObject))success onError:(void (^)())cerror {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PATH_GET_USER_INFO, [NSString stringWithFormat:@"?access_token=%@", self.token]];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response ObjeCt: %@", responseObject);
        //NSDictionary *response = responseObject;
        //if (response [@"access_token"]) {
            //self.token = response[@"access_token"];
            success(responseObject);
        //}
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
        cerror();
    }];
}

- (void)requestId:(void (^)())success {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@", BASE_URL, PATH_REQUESTID];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response Objest: %@", responseObject);
        NSDictionary *response = responseObject;
        self.uuid = response[@"uuid"];
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
}

#pragma mark - GKMatchmakerViewControllerDelegate

// The user has cancelled matchmaking
- (void)matchmakerViewControllerWasCancelled:(GKMatchmakerViewController *)viewController {
    [presentingViewController
     dismissViewControllerAnimated:YES completion:^{
         
     }];
}

// Matchmaking has failed with an error
- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFailWithError:(NSError *)error {
    if (error.code != 13) {
        [presentingViewController
         dismissViewControllerAnimated:YES completion:^{
             
         }];
    }
    NSLog(@"Error finding match: %@", error);
}

// A peer-to-peer match has been found, the game should start
- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFindMatch:(GKMatch *)match {
    [presentingViewController dismissViewControllerAnimated:YES completion:^{
         [(MenuViewController*)presentingViewController startMultiplayerMatch];
    }];
    self.currentMatch = match;
    match.delegate = self;
    if (!matchStarted && match.expectedPlayerCount == 0) {
        NSLog(@"Ready to start match!");
        [self.delegate matchAboutToStart:match];
        [self lookupPlayers];
    }
}

#pragma mark - GCMatchDelegate

// The match received data sent from the player.
- (void)match:(GKMatch *)theMatch didReceiveData:(NSData *)data fromPlayer:(NSString *)playerID {
    if (_currentMatch != theMatch) return;
    
    [delegate match:theMatch didReceiveData:data fromPlayer:playerID];
}

// The player state changed (eg. connected or disconnected)
- (void)match:(GKMatch *)theMatch player:(NSString *)playerID didChangeState:(GKPlayerConnectionState)state {
    if (_currentMatch != theMatch) return;
    
    switch (state) {
        case GKPlayerStateConnected:
            // handle a new player connection.
            NSLog(@"Player connected!");
            
            if (!matchStarted && theMatch.expectedPlayerCount == 0) {
                NSLog(@"Ready to start match!");
                if (!matchStarted && theMatch.expectedPlayerCount) {
                    [delegate matchAboutToStart:theMatch];
                }
                [self lookupPlayers];
            }
                        break;
        case GKPlayerStateDisconnected:
            // a player just disconnected.
            NSLog(@"Player disconnected!");
            matchStarted = NO;
            [delegate matchEnded];
            break;
        case GKPlayerStateUnknown:
            NSLog(@"Player disconnected!");
            break;
    }
}



// The match was unable to connect with the player due to an error.
- (void)match:(GKMatch *)theMatch connectionWithPlayerFailed:(NSString *)playerID withError:(NSError *)error {
    
    if (_currentMatch != theMatch) return;
    
    NSLog(@"Failed to connect to player with error: %@", error.localizedDescription);
    matchStarted = NO;
    [delegate matchEnded];
}

// The match was unable to be established with any players due to an error.
- (void)match:(GKMatch *)theMatch didFailWithError:(NSError *)error {
    
    if (_currentMatch != theMatch) return;
    
    NSLog(@"Match failed with error: %@", error.localizedDescription);
    matchStarted = NO;
    [delegate matchEnded];
}

- (void)lookupPlayers {
    
    NSLog(@"Looking up %lu players...", (unsigned long)_currentMatch.playerIDs.count);
    
    [GKPlayer loadPlayersForIdentifiers:_currentMatch.playerIDs withCompletionHandler:^(NSArray *players, NSError *error) {
        
        if (error != nil) {
            NSLog(@"Error retrieving player info: %@", error.localizedDescription);
            matchStarted = NO;
            [delegate matchEnded];
        } else {
            
            // Populate players dict
            playersDict = [NSMutableDictionary dictionaryWithCapacity:players.count];
            for (GKPlayer *player in players) {
                NSLog(@"Found player: %@", player.alias);
                [playersDict setObject:player forKey:player.playerID];
            }
            [playersDict setObject:[GKLocalPlayer localPlayer] forKey:[GKLocalPlayer localPlayer].playerID];
            
            // Notify delegate match can begin
            matchStarted = YES;
            [delegate matchStarted];
        }
    }];
}

#pragma mark - Oauth helper methods

- (NSString *)loadValueWithKey:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

- (void)setUuid:(NSString *)uuid {
    _uuid = uuid;
    if (_uuid) {
        [self saveValue:_uuid withKey:kUUIDKey];
    }
}

- (void)setSecretPass:(NSString *)secretPass {
    _secretPass = secretPass;
    if (_secretPass ) {
        [self saveValue:_secretPass withKey:kSecretKey];
    }
}

- (void)saveValue:(NSString*)secret withKey:(NSString*)key{
    if (!secret) return;
    NSUserDefaults *defaultuser = [NSUserDefaults standardUserDefaults];
    [defaultuser setObject:secret forKey:key];
    [defaultuser synchronize];
}



@end
