//
//  UIView+Stuff.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/25/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "UIView+Stuff.h"

@implementation UIView (Stuff)

- (UIView *)keyView {
    UIWindow *w = [[UIApplication sharedApplication] keyWindow];
    if (w.subviews.count > 0) {
        return [w.subviews objectAtIndex:0];
    } else {
        return w;
    }
}


@end
