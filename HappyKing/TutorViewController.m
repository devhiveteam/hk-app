//
//  TutorViewController.m
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 29/11/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "TutorViewController.h"


@implementation TutorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *colors = [NSArray arrayWithObjects:
                       [UIColor orangeColor],
                       [UIColor redColor],
                       [UIColor greenColor],
                       [UIColor yellowColor],
                       [UIColor lightGrayColor],
                       [UIColor grayColor],
                       [UIColor cyanColor],
                       [UIColor magentaColor],
                       [UIColor clearColor],
                       [UIColor whiteColor],
                       [UIColor greenColor],
                       nil];
    
    instructions = [NSArray arrayWithObjects:
                             @"1_Cada_quien_3vidas",
                             @"2_Pierde_Vida_Carta_Mas_Baja",
                             @"3_Opcion_Cambiar_Quedarse",
                             @"4_Par_Amigo",
                             @"5_Tercia_Maldita",
                             @"6_La_Reina_No_Se_Cambia",
                             @"7_Sale_King_Las_cartas_se_Abren",
                             @"8_Picture",
                             @"9_A_jugar",
                             nil];
    
    self.scrollView.pagingEnabled = YES;
    
    for (int i = 0; i < instructions.count; i++)
    {
        CGRect frame;
        frame.origin.x = [UIScreen mainScreen].bounds.size.width * i; //self.scrollView.frame.size.width * i;
        //frame.size = [UIScreen mainScreen].bounds.size; //self.scrollView.frame.size;
        frame.size.height = [UIScreen mainScreen].bounds.size.height - 40;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
        
        //UILabel *texto = [[UILabel alloc] initWithFrame: frame];
        //texto.backgroundColor = [colors objectAtIndex:i];
        //[texto setText: [instructions objectAtIndex:i]];
        //[texto setTextAlignment: NSTextAlignmentCenter];
        
        UIImageView *subview = [[UIImageView alloc] initWithFrame:frame];
        [subview setImage:[UIImage imageNamed:[instructions objectAtIndex:i]]];
        
        //UIView *subview = [[UIView alloc] initWithFrame:frame];
        //subview.backgroundColor = [colors objectAtIndex:i];
        //[subview addSubview: texto];
        
        
        [self.scrollView addSubview:subview];
    }
    
    //self.scrollView.contentSize =  CGSizeMake(self.scrollView.frame.size.width * colors.count, self.scrollView.frame.size.height);
    self.scrollView.contentSize =  CGSizeMake([UIScreen mainScreen].bounds.size.width * instructions.count, [UIScreen mainScreen].bounds.size.height - 40);
    self.leftArrow.layer.cornerRadius = 5;
    self.rightArrow.layer.cornerRadius = 5;
}

/**
 *  Cierra la vista del tutorial
 *
 *  @param sender botón encargado de cerrar la vista.
 */

- (IBAction)quitTutorViewPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
/**
 *  Ejecuta la navegación dentro del tutorial, ya sea hacia la izquierda o la derecha, dependiendo de qué botón haya
 *  sido presionado
 *
 *  @param sender Botón que se presionó.
 */
- (IBAction)arrowButtonsPressed:(id)sender {
    UIButton *button = sender;
    NSInteger currentPage = [self calculateCurrentPage];
    if (button.tag == 1) {
        currentPage--;
        if (currentPage < 0) {
            currentPage = 0;
        }
    } else {
        currentPage++;
        if (currentPage >= [instructions count]) {
            currentPage = [instructions count] - 1;
        }
    }
    [self scrollToPage:currentPage];
}
/**
 *  Calcula en qué página se encuentra en dicho momento en base a la posición del scrollView
 *
 *  @return Regresa el número de página que actualmente se está visualizando.
 */

- (NSInteger)calculateCurrentPage {
    NSInteger currentPage = 0;
    CGPoint contentOffset = self.scrollView.contentOffset;
    currentPage = contentOffset.x / [UIScreen mainScreen].bounds.size.width;
    return currentPage;
}

/**
 *  Desplaza la vista del tutorial a la página indicada
 *
 *  @param page número de pagina que será visualizada.
 */
- (void)scrollToPage:(NSInteger)page {
    CGFloat currentOffset = [UIScreen mainScreen].bounds.size.width * page;
    CGPoint contentOffset = self.scrollView.contentOffset;
    contentOffset.x = currentOffset;
    [self.scrollView setContentOffset:contentOffset animated:YES];
    
}

@end