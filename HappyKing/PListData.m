//
//  PListData.m
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 26/09/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "PListData.h"

#define CURRENT_VERSION 2

@implementation PListData

+ (void)initialize:(NSString *)file
{
    NSFileManager *fileManger=[NSFileManager defaultManager];
    NSError *error;
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    
    NSString *doumentDirectoryPath = [pathsArray objectAtIndex:0];
    
    NSString *destinationPath = [doumentDirectoryPath stringByAppendingPathComponent:file];
    
    if ([fileManger fileExistsAtPath:destinationPath])
    {
        NSNumber *version = [NSNumber numberWithInt: CURRENT_VERSION];
        NSNumber *vPList = [PListData getVersionFromFile:file];
        
        if([version isEqualToNumber:vPList])
            return;
        
        [fileManger removeItemAtPath:destinationPath error:&error];
    }
    
    NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:file];
                          
    [fileManger copyItemAtPath:sourcePath toPath:destinationPath error:&error];
}


+ (NSArray *)getItemData:(NSString *)key fromFile:(NSString *) fileName
{
    //NSString *file = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *doumentDirectoryPath = [pathsArray objectAtIndex:0];
    NSString *file = [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:file];
    NSArray *data = [dict objectForKey:key];
    return data;
}


+ (NSDictionary *)getActiveItemWithKey:(NSString *)key fromFile:(NSString *)fileName
{
    NSArray *data = [PListData getItemData:key fromFile:fileName];
    for(NSDictionary *dic in data)
    {
        BOOL unlocked = dic[@"unlocked"] ? [dic[@"unlocked"] boolValue] : YES;
        BOOL active = [dic[@"active"] boolValue];
        if(active && unlocked)
        {
            return dic;
        }
    }
    
    return nil;
}


+ (NSString *)getValueWithKey:(NSString *)key fromElement:(NSString *)element fromFile:(NSString *)fileName
{
    NSDictionary *dic = [PListData getActiveItemWithKey:element fromFile:fileName];
    if(dic == nil)
        return  nil;
    
    NSString *value = [dic valueForKey:key];
    return value;
}

+ (void)saveActiveItemForKey:(NSString *)key withName:(NSString *)name fromFile:(NSString *)fileName
{
    //NSString *file = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *doumentDirectoryPath = [pathsArray objectAtIndex:0];
    NSString *file = [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:file];
    
    NSArray *data = [dict objectForKey:key];
    NSMutableArray *mData = [data mutableCopy];
    
    NSString *auxName;
    NSDictionary *element;
    NSMutableDictionary *mElement;
    for(int i = 0; i < [data count]; i++)
    {
        element = [data objectAtIndex:i];
        mElement = [element mutableCopy];
        auxName = [element objectForKey: @"name"];
        if([auxName isEqualToString:name])
            [mElement setValue:[NSNumber numberWithBool:YES] forKey:@"active"];
        else
            [mElement setValue:[NSNumber numberWithBool:NO] forKey:@"active"];
        
        [mData replaceObjectAtIndex:i withObject:mElement];
    }
    
    [dict setValue:mData forKey:key];

    [dict writeToFile:file atomically: YES];
}

+ (void)saveUnlockedItemForKey:(NSString *)key withName:(NSString *)name fromFile:(NSString *)fileName
{
    //NSString *file = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *doumentDirectoryPath = [pathsArray objectAtIndex:0];
    NSString *file = [doumentDirectoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:file];
    
    NSArray *data = [dict objectForKey:key];
    NSMutableArray *mData = [data mutableCopy];
    
    NSString *auxName;
    NSDictionary *element;
    NSMutableDictionary *mElement;
    for(int i = 0; i < [data count]; i++)
    {
        element = [data objectAtIndex:i];
        mElement = [element mutableCopy];
        auxName = [element objectForKey: @"name"];
        if([auxName isEqualToString:name])
        {
            [mElement setValue:[NSNumber numberWithBool:YES] forKey:@"unlocked"];
            [mData replaceObjectAtIndex:i withObject:mElement];
            break;
        }
    }
    
    [dict setValue:mData forKey:key];
    
    [dict writeToFile:file atomically: YES];
}


+ (NSNumber *)getVersionFromFile:(NSString *)fileName
{
    NSArray *pathsArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *doumentDirectoryPath = [pathsArray objectAtIndex:0];
    NSString *file = [doumentDirectoryPath stringByAppendingPathComponent:fileName];
    
    NSDictionary *root = [NSDictionary dictionaryWithContentsOfFile:file];
    
    NSNumber *version = [root objectForKey:@"Version"];
    
    return (version != nil) ? version : [NSNumber numberWithInt:0];
}

@end