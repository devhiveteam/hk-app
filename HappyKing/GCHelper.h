//
//  ScoresManager.h
//  HappyKing
//
//  Created by Leonardo Cid on 02/07/15.
//  Copyright (c) 2015 Devhive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <GameKit/GameKit.h>

#define OAUTH_CLIENT_ID @"mobileV1"
#define OAUTH_CLIENT_SECRET @"abc123456"

//#define BASE_URL @"http://54.76.33.237:8081/"
#define BASE_URL @"http://52.10.191.79:1337/"
//#define BASE_URL @"http://localhost:1337/"
#define PATH_REGISTER @"api/register"
#define PATH_REQUESTID @"api/request-id"
#define PATH_GET_TOKEN @"oauth/token"
#define PATH_GET_USER_INFO @"api/me"
#define PATH_GET_GOINS @"api/coins"
#define PATH_GET_SAVE @"api/save"

#define kSecretKey @"secret"
#define kUUIDKey @"uuid"

@protocol GCHelperDelegate
- (void)matchStarted;
- (void)matchAboutToStart:(GKMatch*)match;
- (void)matchEnded;
- (void)match:(GKMatch *)match didReceiveData:(NSData *)data
   fromPlayer:(NSString *)playerID;
@end

@interface GCHelper : NSObject<GKMatchmakerViewControllerDelegate, GKMatchDelegate> {
    UIViewController *presentingViewController;
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
    BOOL matchStarted;
}

@property (nonatomic) NSUInteger mainLeaderboardScore;
@property (nonatomic) NSUInteger coins;
@property (nonatomic) NSUInteger diamonds;
@property (nonatomic, strong) NSString *leaderboardIdentifier;
@property (nonatomic, readonly) BOOL gameCenterEnabled;
@property (nonatomic, strong) id <GCHelperDelegate> delegate;


@property (nonatomic, strong) NSString *uuid; //returned by requestId
@property (nonatomic, strong) NSString *secretPass; //returned by register
@property (nonatomic, strong) NSString *token; //returned by token
@property (nonatomic, strong) NSString *refreshToken; //returned by token


@property (retain) GKMatch * currentMatch;

+ (GCHelper*)sharedManager;
- (id)init;
- (void)sendScoreToLeaderboard:(NSUInteger)theScore toLeaderboard:(NSString*)leaderboardId;
- (void)updateScoreAndSend:(NSUInteger)newScore;
- (void)authenticateLocalPlayer:(void(^)(UIViewController *viewController, NSError *error))completionHandler;
- (void)requestId:(void (^)())success;
- (void)registerUser:(void (^)())success;
- (void)getOauthToken:(void (^)())success;
- (void)refreshOauthToken:(void (^)())success;
- (void)updateGCId:(void (^)())success;
- (void)getUserInfo: (void (^)(id responseObject))success onError:(void (^)())cerror;
- (void)incrementCoins:(NSInteger)coins;

- (void)findMatchWithMinPlayers:(int)minPlayers
                     maxPlayers:(int)maxPlayers
                 viewController:(UIViewController *)viewController
                         roomId:(NSUInteger)roomId;

@end
