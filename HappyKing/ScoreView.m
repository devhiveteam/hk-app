//
//  ScoreView.m
//  HappyKing
//
//  Created by Leonardo Cid on 10/24/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "ScoreView.h"
#import "Definitions.h"
#import "GCHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation ScoreView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
    
}
- (void)initialize {
    [self.layer setCornerRadius: 5];
    [self.layer setBorderWidth:2];
    [self.layer setBorderColor:[UIColor yellowColor].CGColor];
    
    self.coinsLabel.font = kMainFontWithSize(14);
    self.diamondsLabel.font = kMainFontWithSize(14);
    
    self.coinsLabel.textColor = [UIColor whiteColor];
    self.diamondsLabel.textColor = [UIColor whiteColor];
    
    self.coinsLabel.text = [@([[GCHelper sharedManager] coins]) stringValue];
    self.diamondsLabel.text = [@([[GCHelper sharedManager] diamonds]) stringValue];
    
    [[GCHelper sharedManager] addObserver:self forKeyPath:@"coins" options:NSKeyValueObservingOptionNew context:nil];
    [[GCHelper sharedManager] addObserver:self forKeyPath:@"diamonds" options:NSKeyValueObservingOptionNew context:nil];
}

+ (ScoreView*)sharedInstance {
    static ScoreView *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [[[NSBundle mainBundle] loadNibNamed:@"ScoreView" owner:nil options:nil] firstObject];
        }
    });
    return instance;
}

- (void)showInView:(UIView*)view {
    CGRect rect = self.frame;
    CGRect viewRect = view.frame;
    
    rect.origin.x = viewRect.size.width - rect.size.width - 10;
    rect.origin.y = viewRect.size.height - rect.size.height - 10;
    
    self.frame = rect;
    [view addSubview:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    id newchange = [change objectForKey:NSKeyValueChangeNewKey];
    if ([keyPath isEqualToString:@"coins"]) {
        
    } else if ([keyPath isEqualToString:@"diamonds"]) {
        
    }
}

@end
