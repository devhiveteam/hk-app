//
//  ThemeStoreViewController.h
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 25/10/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

typedef enum {
    SectionMainTheme = 0,
    SectionStoreTheme
} MenuSectionTheme;

typedef enum {
    MenuTableTheme = 0,
    MenuCardsTheme,
    MenuButtonsTheme,
    MenuStoreTheme,
}MenuEntryTheme;

@protocol themeTableChanged
- (void)delegateEventTableThemeChanged;
@end

@interface ThemesStoreViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    id delegate;
    MenuSectionTheme menuSection;
    NSArray *_products;
    UIFont *cellFont;
    float cellHeight;
    NSArray *menuEntries;
}


@property (nonatomic, strong)IBOutlet UIImageView *backgroundThemeStoreImageView;
@property (nonatomic, strong)IBOutlet UITableView *tableView;

- (void)setDelegate: (id)newDelegate;

@end
