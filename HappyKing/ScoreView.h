//
//  ScoreView.h
//  HappyKing
//
//  Created by Leonardo Cid on 10/24/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreView : UIView
@property (nonatomic, weak) IBOutlet UILabel *coinsLabel;
@property (nonatomic, weak) IBOutlet UILabel *diamondsLabel;
@property (nonatomic, weak) IBOutlet UIImageView *coinsImageView;
@property (nonatomic, weak) IBOutlet UIImageView *diamondsImageView;

- (void)showInView:(UIView*)view;
+ (ScoreView*)sharedInstance;

@end
