//
//  MPRoomsViewController.h
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 07/01/16.
//  Copyright © 2016 Devhive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

@interface MPRoomsViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, GKGameCenterControllerDelegate>
{

}

@property (nonatomic, strong)IBOutlet UIView *mainView;
@property (nonatomic, strong)IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSArray *rooms;
@property (nonatomic) NSInteger players;

@property (nonatomic, strong)IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIButton *quitView;

@end
