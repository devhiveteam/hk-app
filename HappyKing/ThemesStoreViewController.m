//
//  ThemeStoreViewController.m
//  HappyKing
//
//  Created by Aldo Miranda-Aguilar on 25/10/15.
//  Copyright © 2016 Jaime Cohen. All rights reserved.
//

#import "ThemesStoreViewController.h"
#import "ThemesSelectViewController.h"
#import "TutorViewController.h"
#import "PListData.h"
#import "Definitions.h"
#import "MenuTableViewCell.h"
#import "GCHelper.h"
#import "InAppPurchasesHelper.h"

@interface ThemesStoreViewController ()
@property (nonatomic, weak) IBOutlet UIButton *menuButtonThemeStore;
@property (nonatomic, weak) IBOutlet UIButton *cardThemeButton;
@property (nonatomic, weak) IBOutlet UIButton *tableThemeButton;
@property (nonatomic, weak) IBOutlet UIButton *buttonThemeButton;
@property (nonatomic, weak) IBOutlet UIButton *shopButton;

@end

@implementation ThemesStoreViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self layoutSubViews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIFont *font = kMainFontWithSize(18);
    
    self.cardThemeButton.titleLabel.font =  font;
    self.tableThemeButton.titleLabel.font =  font;
    self.buttonThemeButton.titleLabel.font =  font;
    self.shopButton.titleLabel.font = font;
    
    NSString *ImageProfileTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageProfileTheme != nil)
    {
        [self.backgroundThemeStoreImageView setImage : [UIImage imageNamed:ImageProfileTheme]];
    }
    
    menuEntries = [NSArray arrayWithObjects:
                   @{@"title" : @"Comprar Monedas", @"type": @(MenuStoreTheme)},
                   @{@"title" : @"Temas de Mesa", @"type": @(MenuTableTheme)},
                   @{@"title" : @"Temas de Cartas", @"type": @(MenuCardsTheme)},
                   @{@"title" : @"Temas de Botones", @"type": @(MenuButtonsTheme)},
                   nil];
    
    menuSection= SectionMainTheme;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)layoutSubViews {
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat height = [[UIScreen mainScreen] bounds].size.height * .4;
    
    cellHeight = (bounds.size.height - height - 12) / 3;
    
    float width = (cellHeight + 10) * 600.0/204.0;
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:width];
    [self.tableView addConstraint:constraint];
    [self.tableView setNeedsLayout];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch(menuSection) {
        case SectionMainTheme:
            return [menuEntries count];
        case SectionStoreTheme:
            return _products.count + 1;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIFont *font = kMainFontWithSize(18);
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuTableCell"];
    cell.lblTitle.layer.cornerRadius = 5;
    cell.lblTitle.layer.masksToBounds = YES;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblTitle.font = font;
    if (!cellFont) {
        cellFont = cell.lblTitle.font;
    }
    cell.lblTitle.text = @"";
    UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
    [imageView setImage:[UIImage imageNamed:@"Menu_Boton_Blank.png"]];
    cell.lblTitle.highlightedTextColor = [UIColor colorWithRed:0.09 green:0.24 blue:0.1 alpha:1.0];
    
    if (menuSection == SectionMainTheme)
    {
        cell.lblTitle.font = cellFont;
        NSDictionary *dict = menuEntries[indexPath.row];
        switch ((MenuEntryTheme)[dict[@"type"] intValue]) {
            case MenuTableTheme:
                cell.lblTitle.text = @"Temas de Mesa";
                break;
            case MenuCardsTheme:
                cell.lblTitle.text = @"Temas de Cartas";
                break;
            case MenuButtonsTheme:
                cell.lblTitle.text = @"Temas de Botones";
                break;
            case MenuStoreTheme:
                cell.lblTitle.text = @"Comprar Monedas";
        }
    }
    else
    {
        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:2];
        [imageView setImage:[UIImage imageNamed:@"Menu_Boton_Blank.png"]];
        if (indexPath.row == 0) {
            cell.lblTitle.font = cellFont;
            cell.lblTitle.text = @"Atrás";
        } else {
            cell.lblTitle.font = [font fontWithSize:12];
            SKProduct * product = (SKProduct *) _products[indexPath.row - 1];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [formatter setLocale:product.priceLocale];
            NSString *localizedMoneyString = [formatter stringFromNumber:product.price];
            cell.lblTitle.text = [NSString stringWithFormat:@"%@ - %@", product.localizedTitle, localizedMoneyString];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch(menuSection) {
        case SectionMainTheme:
        {
            if (indexPath.row == 1) {
                menuSection = SectionMainTheme;
                ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
                [storeVC setDelegate:self];
                [storeVC setType: @"Tables" withObjectName:@"table"];
                [self presentViewController:storeVC animated:YES completion:nil];
            } else if (indexPath.row == 2) {
                menuSection = SectionMainTheme;
                ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
                [storeVC setType: @"Cards" withObjectName:@"cards"];
                [self presentViewController:storeVC animated:YES completion:nil];
            } else if (indexPath.row == 3) {
                menuSection = SectionMainTheme;
                ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
                [storeVC setType: @"Buttons" withObjectName:@"button"];
                [self presentViewController:storeVC animated:YES completion:nil];
            } else if (indexPath.row == 0) {
                menuSection = SectionStoreTheme;
                if (!_products) {
                    [[InAppPurchasesHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                        if (success) {
                            _products = products;
                            NSMutableArray *indexPaths = [NSMutableArray array];
                            for (int i = 0; i < products.count; i++) {
                                NSIndexPath *path = [NSIndexPath indexPathForRow:i+1 inSection:0];
                                [indexPaths addObject:path];
                            }
                            [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
                        }
                    }];
                }
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
            break;
        case SectionStoreTheme:
        {
            if (indexPath.row == 0) {
                menuSection = SectionMainTheme;
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
            } else {
                SKProduct *product = _products[indexPath.row - 1];
                NSLog(@"Buying %@...", product.productIdentifier);
                [[InAppPurchasesHelper sharedInstance] buyProduct: product];
            }
        }
            break;
    }
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if (menuSection == SectionMainTheme) {
        //if (indexPath.row == 2)
          //  return NO;
    }
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return cellHeight;
}


















// ========================================================================
// ========================================================================
// ========================================================================
// ========================================================================

-(void)delegateEventThemeChanged {
    NSString *ImageProfileTheme = [PListData getValueWithKey:@"table" fromElement:@"Tables" fromFile:@"themes"];
    if(ImageProfileTheme != nil)
    {
        [self.backgroundThemeStoreImageView setImage : [UIImage imageNamed:ImageProfileTheme]];
        [delegate delegateEventTableThemeChanged];
    }
}

- (IBAction)menuButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)cardThemeButtonPressed:(id)sender {
    ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
    [storeVC setType: @"Cards" withObjectName:@"cards"];
    [self presentViewController:storeVC animated:YES completion:^{
        
    }];
}

- (IBAction)tableThemeButtonPressed:(id)sender {
    ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
    [storeVC setDelegate:self];
    [storeVC setType: @"Tables" withObjectName:@"table"];
    [self presentViewController:storeVC animated:YES completion:nil];
}

- (IBAction)buttonThemeButtonPressed:(id)sender {
    ThemesSelectViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemesSelectViewController"];
    [storeVC setType: @"Buttons" withObjectName:@"button"];
    [self presentViewController:storeVC animated:YES completion:^{
        
    }];
    
}

- (IBAction)shopButtonPressed:(id)sender {
    
}

- (void)setDelegate: (id)newDelegate{
    delegate = newDelegate;
}

@end